package com.global.fp;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.next.fb.R;

import android.graphics.Bitmap;

public class MainActivity extends Activity implements View.OnClickListener {
    public static ProgressBar mProgres;
    private TextView mTextBottom;
    public static WebView mWebview;
    private Button mLiveBtn;
    private Button mScoreBtn;
    private Button mMenuBtn;
    public static Button mRefreshBtn;


    private static String url;
    private static String previousUrl;

    private boolean isExit;

    public static void doPageStarted(WebView view) {
        previousUrl = view.getUrl();
        mProgres.setVisibility(View.VISIBLE);
        mRefreshBtn.setVisibility(View.INVISIBLE);
    }

    public static void doPageFinished(WebView view) {
        mProgres.setVisibility(View.INVISIBLE);
        mRefreshBtn.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fb);
        mProgres = ((ProgressBar) findViewById(R.id.progress));
        mTextBottom = ((TextView) findViewById(R.id.textview));
        int orientation = getOrientation();
        if (orientation != 1)
            mTextBottom.setHeight(getBottomHeight(this, 80));
        else
            mTextBottom.setHeight(getBottomHeight(this, 180));

        mWebview = ((WebView) findViewById(R.id.web));
        mWebview.getSettings().setUserAgentString(Constants.MOBILE_USER_AGENT);
        mWebview.getSettings().setAppCacheEnabled(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        mLiveBtn = ((Button) findViewById(R.id.liveBtn));
        mScoreBtn = ((Button) findViewById(R.id.scoresBtn));
        mMenuBtn = ((Button) findViewById(R.id.menuBtn));
        mRefreshBtn = ((Button) findViewById(R.id.refreshBtn));
        mLiveBtn.setOnClickListener(this);
        mScoreBtn.setOnClickListener(this);
        mMenuBtn.setOnClickListener(this);
        mRefreshBtn.setOnClickListener(this);

        url = Constants.URL_SCORE;
        isExit = false;

    }

    private int getOrientation() {
        return getResources().getConfiguration().orientation;
    }

    public int getBottomHeight(Context paramContext, int paramInt) {
        return (int) (paramInt * paramContext.getResources()
                .getDisplayMetrics().density);
    }

    public void onClick(View paramView) {
        switch (paramView.getId()) {
            case R.id.liveBtn:
                mLiveBtn.setBackgroundResource(R.drawable.button_left_y);
                mScoreBtn.setBackgroundResource(R.drawable.button_center_n);
                mMenuBtn.setBackgroundResource(R.drawable.button_right_n);
                url = Constants.URL_LIVE;
                reload();
                break;
            case R.id.scoresBtn:
                mLiveBtn.setBackgroundResource(R.drawable.button_left_n);
                mScoreBtn.setBackgroundResource(R.drawable.button_center_y);
                mMenuBtn.setBackgroundResource(R.drawable.button_right_n);
                url = Constants.URL_SCORE;
                reload();
                break;
            case R.id.menuBtn:
                mLiveBtn.setBackgroundResource(R.drawable.button_left_n);
                mScoreBtn.setBackgroundResource(R.drawable.button_center_n);
                mMenuBtn.setBackgroundResource(R.drawable.button_right_y);
                url = Constants.URL_MENU;
                reload();
                break;
            case R.id.refreshBtn:
                reload();

        }
    }

    private void reload() {
        // onPageStarted(mWebview);
        if (haveNetwork()) {

            mWebview.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon) {
                    doPageStarted(view);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    doPageFinished(view);
                }
            });
            mWebview.loadUrl("about:blank");
            mWebview.clearHistory();
            mWebview.clearView();
            mWebview.loadUrl(url);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.error),
                    0).show();
            mWebview.setVisibility(View.INVISIBLE);

        }
    }

    public boolean haveNetwork() {
        ConnectivityManager localConnectivityManager = (ConnectivityManager) getSystemService("connectivity");
        boolean haveNetwork;
        if ((localConnectivityManager.getActiveNetworkInfo() == null)
                || (!localConnectivityManager.getActiveNetworkInfo()
                .isConnectedOrConnecting()))
            haveNetwork = false;
        else
            haveNetwork = true;
        return haveNetwork;
    }

    @Override
    public void onPause() {
        mWebview.stopLoading();

        super.onPause();
    }

    @Override
    protected void onResume() {
        reload();
        super.onResume();

    }

    @Override
    protected void onStop() {
        mWebview.stopLoading();
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (isExit) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, getString(R.string.press_again_to_exit), 0)
                    .show();
            isExit = true;
            new Handler().postDelayed(new d(this), 4000L);
        }

    }

    class d implements Runnable {
        d(MainActivity paramFbActivity) {
        }

        public void run() {
            isExit = false;
        }
    }

}
