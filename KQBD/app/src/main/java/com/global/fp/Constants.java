package com.global.fp;

public class Constants {
	public static final String MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; U; Android 4.2; en-us; Nexus 4 Build/JOP24G) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
	public static final String URL_LIVE = "http://android.livescore.com/#/soccer/live/";
	public static final String URL_SCORE = "http://android.livescore.com/#/soccer/";
	public static final String URL_MENU = "http://android.livescore.com/#/soccer/menu/";
}
