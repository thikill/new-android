/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.handmark.pulltorefresh.library;

import td.lib.R;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import com.handmark.pulltorefresh.library.internal.LoadingLayout;

public class TdPullToCloseScrollView extends PullToRefreshBase<ScrollView> {

	private boolean overScroll = false;

    public TdPullToCloseScrollView(Context context) {
		super(context);
	}

	public TdPullToCloseScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TdPullToCloseScrollView(Context context, Mode mode) {
		super(context, mode);
	}

	public TdPullToCloseScrollView(Context context, Mode mode, AnimationStyle style) {
		super(context, mode, style);
	}

	@Override
	public final Orientation getPullToRefreshScrollDirection() {
		return Orientation.VERTICAL;
	}
	
	@Override
    protected LoadingLayout createLoadingLayout(Context context,
            com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode, TypedArray attrs) {
        LoadingLayout layout = new LoadingLayout(context, mode, getPullToRefreshScrollDirection(), attrs) {
            @Override
            protected void resetImpl() {
            }
            
            @Override
            protected void releaseToRefreshImpl() {
            }
            
            @Override
            protected void refreshingImpl() {
            }
            
            @Override
            protected void pullToRefreshImpl() {
            }
            
            @Override
            protected void onPullImpl(float scaleOfLayout) {
            }
            
            @Override
            protected void onLoadingDrawableSet(Drawable imageDrawable) {
            }
            
            @Override
            protected int getDefaultDrawableResId() {
                return R.drawable.transparent;
            }
        };
        
        String close = context.getResources().getString(R.string.td_pull_to_close);
        String release = context.getResources().getString(R.string.td_release_to_close);
        layout.setPullLabel(close);
        layout.setRefreshingLabel("");
        layout.setLoadingDrawable(null);
        layout.setReleaseLabel(release);
        
        return layout;
    }
	
	@Override
	protected void onRefreshing(boolean doScroll) {
	    //not scroll back
	    super.onRefreshing(false);
	}
	
	public void setOverScroll(boolean over){
        this.overScroll = over;
    }

	@Override
	protected ScrollView createRefreshableView(Context context, AttributeSet attrs) {
		ScrollView scrollView;
		if (overScroll && VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			scrollView = new InternalScrollViewSDK9(context, attrs);
		} else {
			scrollView = new ScrollView(context, attrs);
		}

		scrollView.setId(td.lib.R.id.scrollview);
		return scrollView;
	}

	@Override
	protected boolean isReadyForPullStart() {
		return mRefreshableView.getScrollY() == 0;
	}

	@Override
	protected boolean isReadyForPullEnd() {
		View scrollViewChild = mRefreshableView.getChildAt(0);
		if (null != scrollViewChild) {
			return mRefreshableView.getScrollY() >= (scrollViewChild.getHeight() - getHeight());
		}
		return false;
	}

	@TargetApi(9)
	final class InternalScrollViewSDK9 extends ScrollView {

		public InternalScrollViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(TdPullToCloseScrollView.this, deltaX, scrollX, deltaY, scrollY,
					getScrollRange(), isTouchEvent);

			return returnValue;
		}

		/**
		 * Taken from the AOSP ScrollView source
		 */
		private int getScrollRange() {
			int scrollRange = 0;
			if (getChildCount() > 0) {
				View child = getChildAt(0);
				scrollRange = Math.max(0, child.getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
			}
			return scrollRange;
		}
	}
}
