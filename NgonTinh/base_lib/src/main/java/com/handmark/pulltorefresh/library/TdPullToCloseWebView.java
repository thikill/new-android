/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.handmark.pulltorefresh.library;

import td.lib.R;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.webkit.WebView;

import com.handmark.pulltorefresh.library.internal.LoadingLayout;

public class TdPullToCloseWebView extends PullToRefreshBase<WebView> {
    boolean overScroll = false;

	public TdPullToCloseWebView(Context context) {
		super(context);
	}

	public TdPullToCloseWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TdPullToCloseWebView(Context context, Mode mode) {
		super(context, mode);
	}

	public TdPullToCloseWebView(Context context, Mode mode, AnimationStyle style) {
		super(context, mode, style);
	}
	
	@Override
	protected LoadingLayout createLoadingLayout(Context context,
	        com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode, TypedArray attrs) {
	    LoadingLayout layout = new LoadingLayout(context, mode, getPullToRefreshScrollDirection(), attrs) {
            @Override
            protected void resetImpl() {
            }
            
            @Override
            protected void releaseToRefreshImpl() {
            }
            
            @Override
            protected void refreshingImpl() {
            }
            
            @Override
            protected void pullToRefreshImpl() {
            }
            
            @Override
            protected void onPullImpl(float scaleOfLayout) {
            }
            
            @Override
            protected void onLoadingDrawableSet(Drawable imageDrawable) {
            }
            
            @Override
            protected int getDefaultDrawableResId() {
                return R.drawable.transparent;
            }
        };
        
        String close = context.getResources().getString(R.string.td_pull_to_close);
        String release = context.getResources().getString(R.string.td_release_to_close);
        layout.setPullLabel(close);
        layout.setRefreshingLabel("");
        layout.setLoadingDrawable(null);
        layout.setReleaseLabel(release);
	    
	    return layout;
	}
	
	@Override
    protected void onRefreshing(boolean doScroll) {
        //not scroll back
        super.onRefreshing(false);
    }

	@Override
	public final Orientation getPullToRefreshScrollDirection() {
		return Orientation.VERTICAL;
	}

	@Override
	protected WebView createRefreshableView(Context context, AttributeSet attrs) {
		WebView webView;
		if (overScroll && VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			webView = new InternalWebViewSDK9(context, attrs);
		} else {
			webView = new WebView(context, attrs);
		}

		webView.setId(td.lib.R.id.webview);
		return webView;
	}
	
	public void setOverScroll(boolean over){
	    this.overScroll = over;
	}

	@Override
	protected boolean isReadyForPullStart() {
		return mRefreshableView.getScrollY() == 0;
	}

	@Override
	protected boolean isReadyForPullEnd() {
		float exactContentHeight = FloatMath.floor(mRefreshableView.getContentHeight() * mRefreshableView.getScale());
		return mRefreshableView.getScrollY() >= (exactContentHeight - mRefreshableView.getHeight());
	}

	@Override
	protected void onPtrRestoreInstanceState(Bundle savedInstanceState) {
		super.onPtrRestoreInstanceState(savedInstanceState);
		mRefreshableView.restoreState(savedInstanceState);
	}

	@Override
	protected void onPtrSaveInstanceState(Bundle saveState) {
		super.onPtrSaveInstanceState(saveState);
		mRefreshableView.saveState(saveState);
	}

	@TargetApi(9)
	final class InternalWebViewSDK9 extends WebView {

		// WebView doesn't always scroll back to it's edge so we add some
		// fuzziness
		static final int OVERSCROLL_FUZZY_THRESHOLD = 2;

		// WebView seems quite reluctant to overscroll so we use the scale
		// factor to scale it's value
		static final float OVERSCROLL_SCALE_FACTOR = 1.5f;

		public InternalWebViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(TdPullToCloseWebView.this, deltaX, scrollX, deltaY, scrollY,
					getScrollRange(), OVERSCROLL_FUZZY_THRESHOLD, OVERSCROLL_SCALE_FACTOR, isTouchEvent);

			return returnValue;
		}

		private int getScrollRange() {
			return (int) Math.max(0, Math.floor(mRefreshableView.getContentHeight() * mRefreshableView.getScale())
					- (getHeight() - getPaddingBottom() - getPaddingTop()));
		}
	}
}
