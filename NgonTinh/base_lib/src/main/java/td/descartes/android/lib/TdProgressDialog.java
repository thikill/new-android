package td.descartes.android.lib;

import td.lib.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;

public class TdProgressDialog extends Dialog {

    public static TdProgressDialog show(Context context, CharSequence title, CharSequence message) {
        return show(context, title, message, false);
    }

    public static TdProgressDialog show(Context context, CharSequence title, CharSequence message,
            boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    public static TdProgressDialog show(Context context, CharSequence title, CharSequence message,
            boolean indeterminate, boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    public static TdProgressDialog show(Context context, CharSequence title, CharSequence message,
            boolean indeterminate, boolean cancelable, OnCancelListener cancelListener) {
        TdProgressDialog dialog = new TdProgressDialog(context);
        dialog.setTitle(title);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        /* The next line will add the ProgressBar to the dialog. */
        //dialog.addContentView(new ProgressBar(context), new LayoutParams(LayoutParams.WRAP_CONTENT,
        //        LayoutParams.WRAP_CONTENT));
        ProgressBar pr = (ProgressBar) LayoutInflater.from(context).inflate(R.layout.td_progressbar, null);
        dialog.addContentView(pr, new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        try {
            dialog.show();
        } catch (Exception e) {
        }

        return dialog;
    }
    
    public static TdProgressDialog show(Context context){
        return show(context, null, null, false, false, null);
    }

    public TdProgressDialog(Context context) {
        super(context, R.style.TdDialog);
    }
    
    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
        };
    }
}