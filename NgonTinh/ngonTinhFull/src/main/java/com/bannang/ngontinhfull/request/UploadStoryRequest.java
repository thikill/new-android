package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Story;

public class UploadStoryRequest extends BaseRequest {
	@XmlTag("story")
	public Story story; 
	public UploadStoryRequest() {
		super(getUrl("story/setstory"));
	}

}
