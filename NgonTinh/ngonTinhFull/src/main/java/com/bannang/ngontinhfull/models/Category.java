package com.bannang.ngontinhfull.models;

import java.util.List;

import android.widget.TextView;

import com.bannang.ngontinhfull.DownloadTask;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DatabaseField;

public class Category implements Comparable<Category> {
	@XmlTag("id")
	@DatabaseField(generatedId = true)
	public Integer id;
	@XmlTag("name")
	@DatabaseField
	public String name;
	public DownloadTask task = null;
	public String strText = null;
	public int iconId = 0;
	public int color = -1;
	public TextView tv;
	public Integer firstBookId = 0;

	@Override
	public int compareTo(Category another) {
		return Constants.COLLATOR.getCollationKey(this.name).compareTo(
				Constants.COLLATOR.getCollationKey(another.name));
	}
}
