package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class Download {
	@XmlTag("id")
	@DatabaseField(generatedId=true)
	public Integer id;
	@XmlTag("bookId")
	@DatabaseField(columnName = "book_id")
	public Integer bookId;


}
