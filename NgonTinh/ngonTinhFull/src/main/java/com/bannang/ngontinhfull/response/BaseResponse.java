package com.bannang.ngontinhfull.response;

import java.io.InputStream;

import org.apache.http.Header;

import com.bannang.ngontinhfull.common.XmlTag;

public class BaseResponse {
	@XmlTag("code")
	public String code;
	@XmlTag("message")
	public String message;
	public Header[] headers;
	public InputStream rawData;
	
	public boolean isSuccess(){
	    return "0".equals(code);
	}
}
