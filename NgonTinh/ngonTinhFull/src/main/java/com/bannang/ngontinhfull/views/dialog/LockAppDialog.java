package com.bannang.ngontinhfull.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;

public class LockAppDialog extends Dialog implements
		android.view.View.OnClickListener {

	private Button mBtnCancel;
	private Button mBtnOK;

	private EditText mEditPassword;
	private EditText mEditConfirmPassword;
	private TextView mTvErrorMessage;
	private CheckBox mChkResetPwd;

	public LockAppDialog(Context context) {
		super(context);
		setContentView(R.layout.lock_app_dlg);
		setCancelable(true);
		setCanceledOnTouchOutside(false);

		mBtnCancel = (Button) findViewById(R.id.btnCancel);
		mBtnOK = (Button) findViewById(R.id.btnOK);

		mEditPassword = (EditText) findViewById(R.id.editPassword);
		mEditConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);
		mChkResetPwd = (CheckBox) findViewById(R.id.chkResetPassword);
		mTvErrorMessage = (TextView) findViewById(R.id.tvError);

		mBtnCancel.setOnClickListener(this);
		mBtnOK.setOnClickListener(this);
		mChkResetPwd.setOnClickListener(this);
		setTitle(R.string.create_password);

	}

	@Override
	public void onClick(View v) {
		InputMethodManager imm = (InputMethodManager) getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditPassword.getWindowToken(), 0);

		if (mLockAppInterface == null)
			return;
		boolean isChk = mChkResetPwd.isChecked();
		if (v == mChkResetPwd) {
			if (isChk == true) {
				mEditPassword.setEnabled(false);
				mEditConfirmPassword.setEnabled(false);
			} else {
				mEditPassword.setEnabled(true);
				mEditConfirmPassword.setEnabled(true);
			}
			return;
		}
		if (v == mBtnCancel) {
			dismiss();
			return;
		}
		if (v == mBtnOK) {
			String password = mEditPassword.getText().toString();
			String confirmPassword = mEditConfirmPassword.getText().toString();
			Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
			if (isChk == false) {
				if ("".equals(password) == true) {
					mTvErrorMessage.setText(R.string.unlock_err_01);
					mTvErrorMessage.setVisibility(View.VISIBLE);
				} else if ("".equals(confirmPassword) == true) {
					mTvErrorMessage.setText(R.string.unlock_err_02);
					mTvErrorMessage.setVisibility(View.VISIBLE);
				} else if (password.equals(confirmPassword) == false) {
					mTvErrorMessage.setText(R.string.unlock_err_03);
					mTvErrorMessage.setVisibility(View.VISIBLE);

				} else {
					// mTvErrorMessage.setVisibility(View.VISIBLE);

					editor.putString("password", password);
					editor.putString("passwordvd", confirmPassword);
					editor.commit();
					dismiss();
				}
			} else {
				editor.putString("password", "");
				editor.putString("passwordvd", "");
				editor.commit();
				dismiss();
			}
		}
	}

	private LockAppInterface mLockAppInterface;

	public void setmLockAppInterface(LockAppInterface mLockAppInterface) {
		this.mLockAppInterface = mLockAppInterface;
	}

	public static interface LockAppInterface {
		public void cancelLockApp();

		public void okLockApp();
	}

}
