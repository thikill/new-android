package com.bannang.ngontinhfull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.loader.ImageLoader;
import com.bannang.ngontinhfull.models.Page;
import com.bannang.ngontinhfull.models.PartnerConfig;
import com.bannang.ngontinhfull.models.StartAppConfig;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;

public class Static {
	public static Tracker TRACKER;
	public static GoogleAnalytics sGaInstance;
	static final String GA_ID = "UA-43873441-4";

	public static int sConnectTimeout = 5000;
	public static int sSokectTimeout = 5000;
	private static DatabaseHelper _mDb = null;

	public static float textSize = 18.0f;
	private static ScreenInfo sScreenInfo = null;
	public static List<DownloadTask> tasks = new ArrayList<DownloadTask>();

	public static ImageLoader _imageLoader = null;
	public static Page currentStartPage = null;

	public static ImageLoader getIL(Context context) {
		if (_imageLoader == null)
			_imageLoader = new ImageLoader(context, true, Constants.DOWNLOAD_FOLDER+"/cache");
		return _imageLoader;
	}

	public static StartAppConfig getInfo() {
		if (info == null)
			info = new StartAppConfig();
		return info;
	}
	public static void setInfo(StartAppConfig info) {
		Static.info = info;
	}

	private static StartAppConfig info = null;
	private static List<PartnerConfig> partnerConfig = null;

	public static List<PartnerConfig> getPartnerConfig() {
		return partnerConfig;
	}

	public static void setPartnerConfig(List<PartnerConfig> partnerConfig) {
		Static.partnerConfig = partnerConfig;
	}

	public static void release() {
		_mDb = null;
		sScreenInfo = null;
		TRACKER = null;
		sGaInstance = null;
		animShow = null;
	}
	
	public static void showFor(final Toast aToast,
			final long durationInMilliseconds) {

		aToast.setDuration(Toast.LENGTH_SHORT);

		Thread t = new Thread() {
			long timeElapsed = 0l;

			public void run() {
				try {
					while (timeElapsed <= durationInMilliseconds) {
						long start = System.currentTimeMillis();
						aToast.show();
						sleep(1750);
						timeElapsed += System.currentTimeMillis() - start;
					}
				} catch (InterruptedException e) {
				}
			}
		};
		t.start();
	}	

	public static void cancellAllTask() {
		while (tasks.size() > 0)
			tasks.remove(0).cancel(true);
	}

	private static String deviceid = null;

	public static String getDeviceId(Context ctx) {
		if (deviceid == null) {
			deviceid = Secure.getString(ctx.getContentResolver(),
					Secure.ANDROID_ID);
		}
		return deviceid;
	}

	public static DatabaseHelper getDB(final Context context) {
		if (_mDb == null) {
			_mDb = new DatabaseHelper(context, "ngontinhfull");

			try {
				_mDb.initDb(false);
				_mDb.createUserTable();
				//_mDb.resetDB();

			} catch (Exception e) {

			}
		}

		return _mDb;
	}
	public static Animation animShow = null;

	public static Animation getShowViewAnimation(Context context) {
		if (animShow == null)
			animShow = AnimationUtils.loadAnimation(context,
					R.anim.ani_show_left);
		return animShow;

	}

	public static ScreenInfo getScreenInfo(Context act) {
		if (sScreenInfo == null) {
			sScreenInfo = initScreen(act, false);
		}
		return sScreenInfo;
	}

	public static ScreenInfo initScreen(Context act, boolean fullScreen) {
		final DisplayMetrics dm = new DisplayMetrics();
		WindowManager windowManager = ((Activity) act).getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		if (fullScreen)
			((Activity) act).getWindow().addFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		ScreenInfo info = new ScreenInfo();
		display.getMetrics(dm);

		info.width = dm.widthPixels;// display.getWidth();
		info.height = dm.heightPixels;// display.getHeight(); //-

		return info;
	}

	public static class ScreenInfo {
		public int width;
		public int height;
	}

	public static byte[] decompress(byte[] compressed) throws IOException {
		final int BUFFER_SIZE = 32;
		ByteArrayInputStream is = new ByteArrayInputStream(compressed);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		GZIPInputStream gis = new GZIPInputStream(is, BUFFER_SIZE);

		byte[] data = new byte[BUFFER_SIZE];
		int bytesRead;
		while ((bytesRead = gis.read(data)) != -1) {
			os.write(data, 0, bytesRead);
		}
		gis.close();
		is.close();
		return os.toByteArray();
	}

}
