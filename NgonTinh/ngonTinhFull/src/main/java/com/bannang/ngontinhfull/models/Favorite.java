package com.bannang.ngontinhfull.models;

import java.sql.Date;

import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DatabaseField;

public class Favorite {
	@XmlTag("id")
	@DatabaseField(generatedId = true)
	public Integer id;
	@XmlTag("bookId")
	@DatabaseField(columnName = "book_id")
	public Integer bookId;
	@XmlTag("readTime")
	@DatabaseField(columnName = "read_time")
	public Date readTime;

}
