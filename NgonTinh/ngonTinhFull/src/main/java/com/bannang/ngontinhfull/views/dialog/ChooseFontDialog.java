package com.bannang.ngontinhfull.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;

public class ChooseFontDialog extends Dialog implements OnCheckedChangeListener {
	private RadioGroup mFontGroup;

	public ChooseFontDialog(Context context) {
		super(context);
		setContentView(R.layout.choose_font_size_dlg);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		setTitle(R.string.font_title);
		mFontGroup = (RadioGroup) findViewById(R.id.fontGroup);
		for (int i = 0; i < mFontGroup.getChildCount(); i++) {
			RadioButton rd = (RadioButton) mFontGroup.getChildAt(i);
			if (rd == null)
				continue;
			rd.setOnCheckedChangeListener(this);
			float textSize = rd.getTextSize()
					/ context.getResources().getDisplayMetrics().density;
			if (textSize - 1.0 <= Static.textSize
					&& Static.textSize <= 1.0 + textSize) {
				rd.setChecked(true);
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		dismiss();
		if (mInterface != null)
			if (isChecked) {
				float textSize = buttonView.getTextSize()
						/ getContext().getResources().getDisplayMetrics().density;
				mInterface.onChooseFont(textSize);
			}
	}

	private ChooseFontInterface mInterface;

	public void setInterface(ChooseFontInterface _interface) {
		mInterface = _interface;
	}

	public static interface ChooseFontInterface {
		public void onChooseFont(float font);
	}

}
