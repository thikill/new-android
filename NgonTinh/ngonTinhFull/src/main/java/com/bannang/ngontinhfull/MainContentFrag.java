package com.bannang.ngontinhfull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragment;
import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.MainActivity;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Category;
import com.bannang.ngontinhfull.models.History;
import com.bannang.ngontinhfull.models.Story;
import com.bannang.ngontinhfull.request.DownloadChaptersRequest;
import com.bannang.ngontinhfull.views.BookView;
import com.bannang.ngontinhfull.views.DownloadView;
import com.bannang.ngontinhfull.views.FavoriteView;
import com.bannang.ngontinhfull.views.FoodViewItem;
import com.bannang.ngontinhfull.views.HistoryView;
import com.bannang.ngontinhfull.views.StartupPage;
import com.bannang.ngontinhfull.views.DownloadView.DVInterface;
import com.bannang.ngontinhfull.views.FavoriteView.FVInterface;
import com.bannang.ngontinhfull.views.HistoryView.HVInterface;
import com.bannang.ngontinhfull.views.StartupPage.StartupPageInterface;
import com.j256.ormlite.dao.Dao;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.startapp.android.publish.banner.Banner;

public class MainContentFrag extends SherlockFragment implements
		StartupPageInterface, HVInterface, DVInterface, FVInterface {

	private Category mCat;
	private ProgressBar mLoading;

	private StartupPage mStartupPage;
	private BookView mBookView;
	private LinearLayout mLinearContainer;
	private static final int REQUEST_CODE = 1234;
	public static MainContentFrag INSTANCE;
	private Banner mBanner;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.content_frame, null);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		View mainView = getView();
		INSTANCE = this;
		mLoading = (ProgressBar) mainView.findViewById(R.id.progressBarLoading);
		mStartupPage = (StartupPage) mainView.findViewById(R.id.startupPage);
		mStartupPage.setInterface(this);
		mLinearContainer = (LinearLayout) mainView
				.findViewById(R.id.linearMainContainer);
		mBanner = (Banner) mainView.findViewById(R.id.startAppBanner);
		loadStory();
		if (Static.getInfo().getStartAppEnable4()) {
			mBanner.showBanner();
		} else {
			mBanner.hideBanner();
		}
	}

	public void setData(Category cat) {
		mCat = cat;
		if (getActivity() == null)
			return;
		loadStory();
	}

	private void loadStory() {
		if (mCat == null)
			return;
		if (mBookView == null)
			mBookView = new BookView(getActivity());
		mLinearContainer.removeAllViews();
		onInvalidOptionsMenu(1);
		mLinearContainer.addView(mBookView);

		if (mCat == null)
			return;
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Story>> task = new AsyncTask<Void, Void, List<Story>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Story> doInBackground(Void... params) {
				try {
					Dao<Story, Integer> storyDao = Static.getDB(getActivity())
							.getStoryDao();
					if (mCat.id != -1) {
						return storyDao.query(storyDao.queryBuilder().where()
								.eq("category_id", mCat.id).prepare());
					} else {
						return storyDao.queryForAll();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Story> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0)
					return;
				Collections.sort(result);
				loadList(result);
			}

		};
		Utils.executeAsyncTask(task);

	}

	public void loadList(List<Story> stories) {
		mBookView.mListViewBooks.setAdapter(new ArrayAdapter<Story>(
				getActivity(), android.R.layout.simple_list_item_1, stories) {
			@Override
			public View getView(final int position, View convertView,
					ViewGroup parent) {
				FoodViewItem fvi = (FoodViewItem) convertView;
				if (fvi == null)
					fvi = new FoodViewItem(getActivity());
				fvi.setData(getItem(position));
				fvi.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						startReadStory(getItem(position));
					}
				});
				return fvi;
			}
		});
	}

	public void startReadStory(Story story) {

		try {
			Dao<History, Integer> historyDao = Static.getDB(getActivity())
					.getHistoryDao();
			History history = historyDao.queryForFirst(historyDao
					.queryBuilder().where().eq("book_id", story.id).prepare());

			if (history == null)
				history = new History();
			else {
				history.readCount++;
			}
			history.bookId = story.id;
			history.readTime = new Date(System.currentTimeMillis());
			historyDao.createOrUpdate(history);
			Intent intent = new Intent(getActivity(), ReaderActivity.class);
			intent.putExtra(ReaderActivity.KEY_CATEGORY, story.categoryId);
			intent.putExtra(ReaderActivity.KEY_STORY, story.id);
			intent.putExtra(ReaderActivity.KEY_STORY_CHAP, history.curChapId);
			intent.putExtra(ReaderActivity.KEY_STORY_START,
					history.curCharIndex);
			startActivity(intent);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onCategory() {
		((SlidingFragmentActivity) getActivity()).getSlidingMenu().showMenu(
				true);
	}

	private HistoryView mHistoryView = null;
	private FavoriteView mFavoriteView = null;
	private DownloadView mDownloadView = null;
	private AlertDialog show;

	@Override
	public void onHistory() {
		if (mHistoryView == null) {
			mHistoryView = new HistoryView(getActivity());
			mHistoryView.setInterface(this);
		}
		mHistoryView.loadList();
		mLinearContainer.removeAllViews();
		mLinearContainer.addView(mHistoryView);
		onInvalidOptionsMenu(2);
		((MainActivity) getActivity()).getSupportActionBar().setTitle(
				R.string.history);
	}

	@Override
	public void onFavorite() {
		if (mFavoriteView == null) {
			mFavoriteView = new FavoriteView(getActivity());
			mFavoriteView.setInterface(this);
		}
		mLinearContainer.removeAllViews();
		mLinearContainer.addView(mFavoriteView);
		onInvalidOptionsMenu(2);
		((MainActivity) getActivity()).getSupportActionBar().setTitle(
				R.string.favorite);
	}

	public void showDashBoard() {
		if (mStartupPage == null)
			mStartupPage = new StartupPage(getActivity());
		mLinearContainer.removeAllViews();
		mLinearContainer.addView(mStartupPage);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE) {
			// Populate the wordsList with the String values the recognition
			// engine thought it heard
			if (data != null) {
				ArrayList<String> matches = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if (matches != null)
					mStartupPage.mTvAutoSearch.setText(matches.get(0));
			}
		}
	}

	@Override
	public void onReadBook(Story story) {
		startReadStory(story);
	}

	@Override
	public void onSelectBook(Story story) {
		startReadStory(story);
	}

	@Override
	public void onShowAll() {
		Category cat = new Category();
		cat.id = -1;
		setData(cat);
	}

	@Override
	public void onGoStore() {
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(getString(R.string.PlayStoreURI)));

		startActivity(intent);

	}

	@Override
	public void onDataDownload() {
		if (mDownloadView == null) {
			mDownloadView = new DownloadView(getActivity());
			mDownloadView.setInterface(this);
		}
		mDownloadView.loadList();
		mLinearContainer.removeAllViews();
		mLinearContainer.addView(mDownloadView);
		onInvalidOptionsMenu(2);
		((MainActivity) getActivity()).getSupportActionBar().setTitle(
				R.string.offline_story);
	}

	private void downloadCategory(final Category cat) {
		// if (cat.needDowload == null || cat.needDowload.size() == 0
		// || cat.tv == null) {
		// return;
		// } else
		if (cat.task != null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
					.setIcon(R.drawable.ic_app)
					.setTitle("Dung download")
					.setMessage("Ban co muon dung download khong?")
					.setPositiveButton(R.string.Application_ConfirmExitOK,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									cat.task.isCancelled = true;
									cat.task.cancel(true);
									cat.task = null;
									cat.strText = cat.name;
									cat.color = Color.BLACK;
									cat.tv.setText(cat.strText);
									cat.tv.setTextColor(cat.color);
								}
							})
					.setNegativeButton(R.string.Application_ConfirmExitCancel,
							null);
			builder.create().show();
			return;
		}
		cat.strText = cat.name + "Waiting...";
		cat.color = Color.RED;
		cat.tv.setTextColor(cat.color);
		cat.task = new DownloadTask() {

			@Override
			protected void onPreExecute() {

				isCancelled = false;
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				DownloadChaptersRequest dcr = new DownloadChaptersRequest();

				// try {
				// while (cat.needDowload.size() > 0) {
				// if (isCancelled() || isCancelled) {
				// isCancelled = false;
				// return false;
				// }
				// Integer id = cat.needDowload.get(0);
				//
				// URL url = new URL(
				// "https://dl.dropboxusercontent.com/u/223793945/chapter/"
				// + id);
				//
				// URLConnection conn = url.openConnection();
				// conn.setConnectTimeout(Static.sConnectTimeout);
				//
				// byte[] buffer = new byte[1024];
				// int len = 0;
				// File f = new File(Environment
				// .getExternalStorageDirectory()
				// .getAbsolutePath()
				// + "/dt");
				// if (!f.exists())
				// f.mkdir();
				// f = new File(f.getAbsolutePath() + "/c");
				// if (!f.exists())
				// f.mkdir();
				// f = new File(f.getAbsolutePath() + "/" + id);
				// FileOutputStream fos = new FileOutputStream(f);
				// InputStream in = conn.getInputStream();
				// while ((len = in.read(buffer)) > 0) {
				// fos.write(buffer, 0, len);
				// }
				// cat.needDowload.remove(0);
				// fos.close();
				// in.close();
				// cat.chapDownloadedIds.add(id);
				// cat.strText = cat.name
				// + " ("
				// + cat.chapDownloadedIds.size()
				// + "/"
				// + (cat.chapDownloadedIds.size() + cat.needDowload
				// .size()) + ") Downloading...";
				// publishProgress(cat.strText);
				//
				// }
				// return true;
				// } catch (Exception e) {
				// mEx = e;
				// e.printStackTrace();
				// }
				return false;
			}

			@Override
			protected void onProgressUpdate(String... values) {
				if (values == null || values.length == 0)
					return;
				cat.tv.setText(values[0]);
			}

			@Override
			protected void onPostExecute(Boolean result) {

				isCancelled = false;

				// if (cat.needDowload.size() == 0) {
				//
				// cat.iconId = R.drawable.downloaded;
				// } else {
				// cat.iconId = R.drawable.download;
				// }
				// cat.strText = cat.name
				// + " ("
				// + cat.chapDownloadedIds.size()
				// + "/"
				// + (cat.chapDownloadedIds.size() + cat.needDowload
				// .size()) + ")";
				// cat.color = Color.BLACK;
				// cat.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,
				// cat.iconId, 0);
				// cat.tv.setText(cat.strText);
				// cat.tv.setTextColor(cat.color);
				// cat.task = null;
				// if (mEx == null)
				// return;
				// if (mEx instanceof UnknownHostException) {
				// Toast.makeText(getActivity(),
				// "Please re-check your network connection",
				// Toast.LENGTH_LONG).show();
				// }

			}
		};
		cat.task.execute();
		Static.tasks.add(cat.task);
	}

	private void loadStory(final String author) {
		if (mCat == null)
			return;
		if (mBookView == null)
			mBookView = new BookView(getActivity());
		mLinearContainer.removeAllViews();
		// onInvalidOptionsMenu(0);
		mLinearContainer.addView(mBookView);

		if (mCat == null)
			return;
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Story>> task = new AsyncTask<Void, Void, List<Story>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Story> doInBackground(Void... params) {
				try {
					Dao<Story, Integer> storyDao = Static.getDB(getActivity())
							.getStoryDao();
					if (mCat.id != -1) {
						return storyDao.query(storyDao.queryBuilder().where()
								.eq("category_id", mCat.id).and()
								.eq("author", author).prepare());
					} else {
						return storyDao.query(storyDao.queryBuilder().where()

						.eq("author", author).prepare());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Story> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0)
					return;
				Collections.sort(result);
				loadList(result);
			}

		};
		Utils.executeAsyncTask(task);

	}

	public void onAuthor() {

		AsyncTask<Void, Void, List<Story>> task = new AsyncTask<Void, Void, List<Story>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Story> doInBackground(Void... params) {
				try {
					Dao<Story, Integer> storyDao = Static.getDB(getActivity())
							.getStoryDao();
					if (mCat.id != -1) {
						return storyDao.query(storyDao.queryBuilder().where()
								.eq("category_id", mCat.id).prepare());
					} else {
						return storyDao.queryForAll();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Story> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0)
					return;
				Set<String> authors = new HashSet<String>();
				for (Story story : result) {
					// story.author="".equals(story.author) ?
					// getString(R.string.author_default):story.author;
					authors.add(story.author);
				}
				final List<String> authorList = new ArrayList<String>(authors);

				Collections.sort(authorList, Constants.COLLATOR);

				final String[] items = new String[authorList.size()];

				int count = 0;
				for (String value : authorList) {

					items[count] = value;
					count++;

				}
				/*
				 * Arrays.sort(items,
				 * java.text.Collator.getInstance(java.util.Locale.FRENCH));
				 */
				android.app.AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle(R.string.author);
				builder.setItems(items, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						String author = authorList.get(which);
						// author=(getString(R.string.author_default)).equals(author)
						// ? "":author;
						loadStory(author);
						dialog.dismiss();
					}
				});
				builder.setNegativeButton(R.string.common_cacel,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}

		};
		Utils.executeAsyncTask(task);
	}

	@Override
	public void onInvalidOptionsMenu(int menuType) {
		((MainActivity) getActivity()).menuType = menuType;
		if (Build.VERSION.SDK_INT >= 11) {
			((MainActivity) getActivity()).invalidateOptionsMenu();
		}

	}

}
