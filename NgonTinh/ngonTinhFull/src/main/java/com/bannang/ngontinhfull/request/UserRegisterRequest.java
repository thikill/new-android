package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.User;

public class UserRegisterRequest extends BaseRequest {
	@XmlTag("user")
	public User user;

	public UserRegisterRequest() {
		super(getUrl("user/register"));
	}

}
