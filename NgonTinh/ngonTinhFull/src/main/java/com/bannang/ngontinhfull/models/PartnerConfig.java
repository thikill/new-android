package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.CSUtil;
import com.bannang.ngontinhfull.common.XmlTag;



public class PartnerConfig {
	@XmlTag("code")
	public String code = "0";
	@XmlTag("name")
	public String name = "0";
	@XmlTag("url")
	public String url = "0";
	@XmlTag("pkgName")
	public String pkgName = "0";
	@XmlTag("isActive")
	public String isActive = "0";
	
	public boolean isActive() {
		return !"0".equals(isActive);
	}
	@XmlTag("isUpdate")
	public String isUpdate = "0";
	public boolean isUpate() {
		return !"0".equals(isUpdate);
	}
	public String toString() {
		try {
			return CSUtil.Object2JSonString(this, null);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	

}