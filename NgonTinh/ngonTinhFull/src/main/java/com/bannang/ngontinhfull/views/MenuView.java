package com.bannang.ngontinhfull.views;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.ViewGroup;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.ReaderActivity;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.GUIs;
import com.bannang.ngontinhfull.models.Chapter;
import com.bannang.ngontinhfull.views.BaseLinearLayout;
import com.bannang.ngontinhfull.views.MenuView.OnChapterSelectedListener;
import com.startapp.android.publish.banner.Banner;

public class MenuView extends BaseLinearLayout implements OnItemClickListener {
	private ListView mLinearMenu;
	public List<Chapter> mChapters;
	private Banner mBanner;

	public MenuView(Context context) {
		super(context, R.layout.menu_view);
		mLinearMenu = (ListView) findViewById(R.id.linearMenu);
		mLinearMenu.setOnItemClickListener(this);
		mBanner = (Banner) findViewById(R.id.startAppBanner);
		if (Static.getInfo().getStartAppEnable6()) {
			mBanner.showBanner();
		} else {
			mBanner.hideBanner();
		}
	}

	public void setData(List<Chapter> chaps) {
		mChapters = chaps;
		loadList();
	}

	private void loadList() {
		if (mChapters == null || mChapters.size() == 0)
			return;

		mLinearMenu.setAdapter(new ArrayAdapter<Chapter>(mContext,
				R.layout.chapter_menu_button, mChapters) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TextView btn = (TextView) convertView;
				if (btn == null) {
					btn = (TextView) inflate(mContext,
							R.layout.chapter_menu_button, null);
				}
				Chapter chap = getItem(position);
				if (chap.name != null && chap.name.length() > 0) {
					btn.setText(Html.fromHtml(chap.name));
				} else {
					btn.setText("Chapter " + (position + 1));
				}
				return btn;
			}
		});
	}

	private OnChapterSelectedListener mOnCategorySelectedListener;

	public void setListener(
			OnChapterSelectedListener mOnCategorySelectedListener) {
		this.mOnCategorySelectedListener = mOnCategorySelectedListener;
	}

	public static interface OnChapterSelectedListener {
		public void onChapterSelected(Chapter chap);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (arg0 == null)
			return;
		Chapter chap = (Chapter) arg0.getItemAtPosition(arg2);
		if (chap == null)
			return;
		if (mOnCategorySelectedListener != null)
			mOnCategorySelectedListener.onChapterSelected(chap);

	}
}
