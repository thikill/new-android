package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.XmlTag;

public class Comment {
	@XmlTag("id")
	public Integer id;
	@XmlTag("chapterid")
	public Integer chapterid;
	@XmlTag("deviceid")
	public String deviceid;
	@XmlTag("content")
	public String content;
	@XmlTag("created_at")
	public String created_at;
}
