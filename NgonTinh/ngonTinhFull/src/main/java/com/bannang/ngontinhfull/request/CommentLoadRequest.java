package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Comment;

public class CommentLoadRequest extends BaseRequest {
	@XmlTag("comment")
	public Comment comment;

	public CommentLoadRequest() {
		super(getUrl("comment/load"));
	}

}
