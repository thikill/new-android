package com.bannang.ngontinhfull.views;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter.CursorToStringConverter;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.MainActivity;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.ConfigUtils;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.PartnerConfig;
import com.bannang.ngontinhfull.models.Story;
import com.bannang.ngontinhfull.views.extension.StorySuggestionCursorAdapter;
import com.startapp.android.publish.banner.banner3d.Banner3D;

public class StartupPage extends BaseLinearLayout implements OnClickListener,
		OnItemClickListener {
	private Button mBtnAll;
	private Button mBtnCategory;
	private Button mBtnHitory;
	private Button mBtnStore;
	private Button mBtnFavorite;
	public AutoCompleteTextView mTvAutoSearch;
	private ImageButton mBtnClear;
	private Button mBtnDownladData;
	private Banner3D mBanner;
	private List<PartnerConfig> listPartner;
	private int nbrPartner;
	static AsyncTask<Void, Void, Void> downloadTask;
	public StartupPage(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public StartupPage(Context context) {
		super(context, R.layout.startup_page);
		initComponent();
	}

	@Override
	protected void onFinishInflate() {
		inflate(mContext, R.layout.startup_page, this);
		super.onFinishInflate();
		initComponent();
		
	}

	private void initComponent() {
		mBtnClear = (ImageButton) findViewById(R.id.btnClear);
		mBtnCategory = (Button) findViewById(R.id.btnCategory);
		mBtnAll = (Button) findViewById(R.id.btnAll);
		mBtnHitory = (Button) findViewById(R.id.btnHistory);
		mBtnStore = (Button) findViewById(R.id.btnStore);
		mBtnFavorite = (Button) findViewById(R.id.btnFavorite);
		mBtnDownladData =(Button)findViewById(R.id.btnDownloadData);
		
		//mBtnDownladData.setEnabled(false);

		mBtnClear.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnHitory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnAll.setOnClickListener(this);
		mBtnStore.setOnClickListener(this);
		
		mBtnDownladData.setOnClickListener(this);

		mTvAutoSearch = (AutoCompleteTextView) findViewById(R.id.autoSearch);
		initAutoSearch();
		downloadFromServer();

	}
	
	public  void updateUI() {
		listPartner = Static.getPartnerConfig();
		nbrPartner = -1;
		if(listPartner == null) return;
		for (int i = 0; i < listPartner.size(); i++) {

			if (ConfigUtils.isPackageExisted(getContext(),
					listPartner.get(i).pkgName) == false) {
				if (listPartner.get(i).isUpate() == false) {
					mBtnStore.setText(listPartner.get(i).name);
				} else {
					mBtnStore.setText(mContext.getString(R.string.update_apk)
							+ " " + listPartner.get(i).name);
				}
				nbrPartner = i;
				break;
			}

		}
	}
	
	
	public  void downloadFromServer() {

		downloadTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				try {
					if (Utils.checkNetworkStatus(mContext) == true) {
						Static.setInfo(ConfigUtils.getStartAppConfig());
						Static.setPartnerConfig(ConfigUtils.getPartnerConfig());
					}

					List<PartnerConfig> listPartner = Static.getPartnerConfig();
					for (PartnerConfig config : listPartner) {
						if (ConfigUtils.isPackageExisted(mContext,
								config.pkgName) == true
								&& config.isUpate() == false) {
							continue;
						}
						File apkfile = new File(Utils.getAppPath()
								+ config.code);
						if (apkfile.exists() == true
								&& config.isUpate() == false) {
							break;
						} else {

							URL url = new URL(config.url);
							URLConnection conn = url.openConnection();
							InputStream is = conn.getInputStream();

							FileOutputStream fos = new FileOutputStream(
									Utils.getAppPath() + config.code);
							byte[] buffer = new byte[1024];
							int len = 0;
							while ((len = is.read(buffer)) > 0) {
								fos.write(buffer, 0, len);
							}
							fos.close();
							is.close();
							break;
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				updateUI();
			}

		};
		Utils.executeAsyncTask(downloadTask);
	}	

	@SuppressLint("DefaultLocale")
	private void initAutoSearch() {
		String[] from = new String[] { StorySuggestionCursorAdapter.STORY_NAME };
		int[] to = new int[] { R.id.AutocompleteTitle };

		StorySuggestionCursorAdapter adapter = new StorySuggestionCursorAdapter(
				getContext(), R.layout.story_autocomplete_line, null, from, to);

		adapter.setCursorToStringConverter(new CursorToStringConverter() {
			public CharSequence convertToString(Cursor cursor) {
				String aColumnString = cursor.getString(cursor
						.getColumnIndex(StorySuggestionCursorAdapter.STORY_NAME));
				return aColumnString;
			}
		});

		adapter.setFilterQueryProvider(new FilterQueryProvider() {
			@Override
			public Cursor runQuery(CharSequence constraint) {

				String inputName = constraint.toString().trim();
				// build your query
				Cursor cursor = Static
						.getDB(mContext)
						.getReadableDatabase()
						.query("story",
								new String[] { "id", "id as _id", "name" },
								"lower(name) like \'%"
										+ inputName.toLowerCase() + "%\'",
								null, null, null, null);

				return cursor;

			}

		});

		mTvAutoSearch.setThreshold(1);
		mTvAutoSearch.setAdapter(adapter);
		mTvAutoSearch.setOnItemClickListener(this);

		mTvAutoSearch
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// Select all when focus gained.
						if (hasFocus) {
							mTvAutoSearch.setSelection(0, mTvAutoSearch
									.getText().length());
						}
					}
				});

		mTvAutoSearch.setCompoundDrawablePadding(5);
	}

	private StartupPageInterface mInterface;

	public void setInterface(StartupPageInterface _interface) {
		mInterface = _interface;
	}

	@Override
	public void onClick(View v) {
		if (mInterface == null)
			return;
		if (v == mBtnClear) {
			mTvAutoSearch.setText("");
			return;
		}
		
		if (v == mBtnCategory) {
			mInterface.onCategory();
			return;
		}
		
		if (v == mBtnAll) {
			mInterface.onShowAll();
			return;
		}
		
		if (v == mBtnStore) {
			if (nbrPartner == -1) {
				mInterface.onGoStore();
			} else {
				if (listPartner == null) return;
				if (ConfigUtils.isPackageExisted(mContext,
						listPartner.get(nbrPartner).pkgName) == true
						&& listPartner.get(nbrPartner).isUpate() == false) {
					try {
						Intent i = mContext.getPackageManager()
								.getLaunchIntentForPackage(
										listPartner.get(nbrPartner).pkgName);
						mContext.startActivity(i);
					} catch (Exception e) {
						// TODO Auto-generated catch block
					}

				} else {

					ConfigUtils.installFromLocal(mContext,
							listPartner.get(nbrPartner).code);

				}

			}
			return;
		}
		
		
		if (v == mBtnHitory) {
			mInterface.onHistory();
			return;
		}
		if (v == mBtnFavorite) {
			mInterface.onFavorite();
			return;
		}
		if(v == mBtnDownladData){
			mInterface.onDataDownload();
			return;
		}
	}

	public class SearchAdapter extends ArrayAdapter<String> {
		private List<String> mListItems;

		public SearchAdapter(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
			mListItems = objects;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView tv = (TextView) convertView;
			if (tv == null)
				tv = new TextView(mContext);
			String name = getItem(position);
			tv.setText(name != null ? name : "");
			return tv;

		}

		@Override
		public String getItem(int position) {
			if (mListItems == null || position >= mListItems.size())
				return null;
			return mListItems.get(position);
		}

		@Override
		public int getCount() {
			return mListItems != null ? mListItems.size() : 0;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Cursor c = ((StorySuggestionCursorAdapter) arg0.getAdapter()).getCursor();
		c.moveToPosition(arg2);
		Story story = new Story();
		story.id = c.getInt(c.getColumnIndex("id"));
		story.name = c.getString(c.getColumnIndex("name"));
		if (mInterface != null)
			mInterface.onSelectBook(story);
	}

	public static interface StartupPageInterface {
		public void onInvalidOptionsMenu(int menuType);
		public void onCategory();
		
		public void onGoStore();
		
		public void onShowAll();
		
		public void onHistory();

		public void onFavorite();


		public void onSelectBook(Story story);
		
		public void onDataDownload();
	}
}
