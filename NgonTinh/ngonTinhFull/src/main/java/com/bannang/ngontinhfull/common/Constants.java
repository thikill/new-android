/*
 * Choi24 Browser for Android
 * 
 * Copyright (C) 2010 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.bannang.ngontinhfull.common;

import java.text.Collator;
import java.util.Locale;

/**
 * Defines constants.
 */
public class Constants {
	static final public String PREFS_PASSWORD = "password";
	static final public String PREFS_PASSWORDVD = "passwordvd";
	public static final int REQUEST_PASSWORD_PROTECTION_CODE = 5;
	public static final Collator COLLATOR = Collator.getInstance(Locale.FRANCE);	
	public static final String DOWNLOAD_FOLDER = "NgonTinhFull";
}
