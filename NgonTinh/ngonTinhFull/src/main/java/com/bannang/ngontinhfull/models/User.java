package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.XmlTag;

public class User {
	@XmlTag("deviceId")
	public String deviceId;
	@XmlTag("name")
	public String name;
}
