/*
 * Choi24 Browser for Android
 * 
 * Copyright (C) 2010 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.bannang.ngontinhfull.views.extension;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

/**
 * Adapter for suggestions.
 */
public class StorySuggestionCursorAdapter extends SimpleCursorAdapter {

	public static final String STORY_NAME = "name";

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            The context.
	 * @param layout
	 *            The layout.
	 * @param c
	 *            The Cursor.
	 * @param from
	 *            Input array.
	 * @param to
	 *            Output array.
	 */
	public StorySuggestionCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to) {
		super(context, layout, c, from, to);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View superView = super.getView(position, convertView, parent);

		return superView;
	}

}
