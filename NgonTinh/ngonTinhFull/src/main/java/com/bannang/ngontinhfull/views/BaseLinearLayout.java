package com.bannang.ngontinhfull.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class BaseLinearLayout extends LinearLayout {
	protected Context mContext;

	public BaseLinearLayout(Context context, int resId) {
		super(context);
		mContext = context;
		inflate(context, resId, this);
	}

	public BaseLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

}
