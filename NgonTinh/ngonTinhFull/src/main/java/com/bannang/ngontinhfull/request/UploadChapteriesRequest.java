package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Chapter;

public class UploadChapteriesRequest extends BaseRequest {
	@XmlTag("chapteries")
	public Chapter[] chapteries; 
	public UploadChapteriesRequest() {
		super(getUrl("chapter/setchapteries"));
	}

}
