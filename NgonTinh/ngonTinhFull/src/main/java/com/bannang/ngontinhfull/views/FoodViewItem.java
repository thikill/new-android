package com.bannang.ngontinhfull.views;

import android.content.Context;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.models.Story;

public class FoodViewItem extends BaseLinearLayout {
	private ImageView mThumbView;
	private TextView mLabel, mAuthor;

	public Story mFood;

	public FoodViewItem(Context context) {
		super(context, R.layout.book_view_item);

		mThumbView = (ImageView) findViewById(R.id.imageFoodThumb);
		mLabel = (TextView) findViewById(R.id.tvFoodLabel);
		mAuthor = (TextView) findViewById(R.id.tvAuthor);
	}

	public void setData(Story food) {
		mFood = food;
		mLabel.setText(food.name);
		String author = food.author;
		if (author != null)
			mAuthor.setText(food.author);
		Static.getIL(mContext).displayImage(
				"https://dl.dropboxusercontent.com/u/223793945/story/"
						+ food.id, mThumbView);
	}

}
