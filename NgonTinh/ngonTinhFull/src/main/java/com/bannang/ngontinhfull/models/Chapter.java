package com.bannang.ngontinhfull.models;

import java.io.Serializable;

import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class Chapter implements Serializable {
	@XmlTag("id")
	@DatabaseField(generatedId = true)
	public Integer id;
	@XmlTag("name")
	@DatabaseField
	public String name;
	@DatabaseField
	public String description;
	
	@DatabaseField(dataType=DataType.BYTE_ARRAY)
	public byte[] content;
	
	@XmlTag("content")
	public String contentCv;
	@XmlTag("story_id")
	@DatabaseField(columnName = "story_id")
	public Integer storyId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
