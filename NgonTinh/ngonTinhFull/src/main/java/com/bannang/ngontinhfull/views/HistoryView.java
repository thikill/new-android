package com.bannang.ngontinhfull.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.History;
import com.bannang.ngontinhfull.models.Story;
import com.j256.ormlite.dao.Dao;

public class HistoryView extends BaseLinearLayout {
	private ListView mListBook;
	private ProgressBar mLoading;

	public HistoryView(Context context) {
		super(context, R.layout.favorite_view);
		mListBook = (ListView) findViewById(R.id.listViewBooks);
		mLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
		loadList();
	}

	public void loadList() {
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Story>> task = new AsyncTask<Void, Void, List<Story>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
			}

			@Override
			protected List<Story> doInBackground(Void... params) {
				try {
					Dao<History, Integer> historyDao = Static.getDB(mContext)
							.getHistoryDao();
					List<History> histories = historyDao.queryForAll();
					List<Integer> bookIds = new ArrayList<Integer>();

					for (History history : histories) {
						bookIds.add(history.bookId);
					}
					Dao<Story, Integer> storyDao = Static.getDB(mContext)
							.getStoryDao();
					return storyDao.query(storyDao.queryBuilder().where()
							.in("id", bookIds).prepare());

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Story> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0) {
					return;
				}
				loadList(result);
			}
		};
		Utils.executeAsyncTask(task);
	}

	public void loadList(List<Story> stories) {
		mListBook.setAdapter(new ArrayAdapter<Story>(mContext,
				android.R.layout.simple_list_item_1, stories) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				FoodViewItem fvi = (FoodViewItem) convertView;
				if (fvi == null)
					fvi = new FoodViewItem(getContext());
				final Story story = (Story) getItem(position);
				fvi.setData(story);
				
				fvi.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mInterface != null)
							mInterface.onReadBook(story);
					}
				});
				return fvi;
			}
		});
	}

	private HVInterface mInterface;

	public void setInterface(HVInterface _interface) {
		mInterface = _interface;
	}

	public static interface HVInterface {
		public void onReadBook(Story story);
	}
}
