package com.bannang.ngontinhfull.common;


import java.io.File;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.Static.ScreenInfo;
import com.bannang.ngontinhfull.common.Constants;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class Utils {
	/**
	 * Check if the SD card is available. Display an alert if not.
	 * 
	 * @param context
	 *            The current context.
	 * @param showMessage
	 *            If true, will display a message for the user.
	 * @return True if the SD card is available, false otherwise.
	 */
	public static boolean checkCardState(Context context, boolean showMessage) {
		// Check to see if we have an SDCard
		String status = Environment.getExternalStorageState();
		if (!status.equals(Environment.MEDIA_MOUNTED)) {

			int messageId;

			// Check to see if the SDCard is busy, same as the music app
			if (status.equals(Environment.MEDIA_SHARED)) {
				messageId = R.string.Commons_SDCardErrorSDUnavailable;
			} else {
				messageId = R.string.Commons_SDCardErrorNoSDMsg;
			}

			if (showMessage) {
				GUIs.showErrorDialog(context,
						R.string.Commons_SDCardErrorTitle, messageId);
			}

			return false;
		}

		return true;
	}

	@TargetApi(11)
	static public <T> void executeAsyncTask(AsyncTask<T, ?, ?> task,
			T... params) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		} else {
			task.execute(params);
		}
	}

	public static void mkAppFoler() {

		File directory = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/" + Constants.DOWNLOAD_FOLDER + "/");
		if (!directory.exists()) {
			try {
				directory.mkdir();
			} catch (Exception e) {

			}
		}
	}

	public static String getAppPath() {

		return Environment.getExternalStorageDirectory().getAbsolutePath()
				+ "/" + Constants.DOWNLOAD_FOLDER + "/";

	}

	public static boolean checkNetworkStatus(Context context) {

		ConnectivityManager connectivityManager;

		boolean connected = false;

		try {

			connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo networkInfo = connectivityManager
					.getActiveNetworkInfo();
			connected = networkInfo != null && networkInfo.isAvailable()
					&& networkInfo.isConnected();

		} catch (Exception e) {
			System.out
					.println("CheckConnectivity Exception: " + e.getMessage());
			Log.v("connectivity", e.toString());
		}

		return connected;

	}
}
