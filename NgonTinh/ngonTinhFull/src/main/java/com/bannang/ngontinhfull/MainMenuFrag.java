package com.bannang.ngontinhfull;

import java.sql.SQLException;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.MainMenuFrag.OnCategorySelectedListener;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Category;
import com.bannang.ngontinhfull.models.Story;
import com.bannang.ngontinhfull.views.CategoryViewItem;
import com.j256.ormlite.dao.Dao;

public class MainMenuFrag extends Fragment implements OnItemClickListener {
	private ListView mLinearMenu;
	private ProgressBar mLoading;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.menu_frame, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mLinearMenu = (ListView) getView().findViewById(R.id.linearMenu);
		mLinearMenu.setOnItemClickListener(this);
		mLinearMenu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mLoading = (ProgressBar) getView()
				.findViewById(R.id.progressBarLoading);
		loadMenu();
	}

	private void loadMenu() {
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Category>> task = new AsyncTask<Void, Void, List<Category>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Category> doInBackground(Void... params) {
				try {
					Dao<Category, Integer> categoryDao = Static.getDB(
							getActivity()).getCategoryDao();
					return categoryDao.queryForAll();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Category> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0)
					return;
				// Collections.sort(result);

				mLinearMenu.setAdapter(new ArrayAdapter<Category>(
						getActivity(), R.layout.main_menu_button, result) {
					@Override
					public View getView(int position, View convertView,
							ViewGroup parent) {
						TextView btn = (TextView) convertView;
						if (btn == null)
							btn = (TextView) View.inflate(getActivity(),
									R.layout.main_menu_button, null);
						btn.setText(getItem(position).name);
						return btn;
					}
				});

			}
		};
		Utils.executeAsyncTask(task);
	}

	private OnCategorySelectedListener mOnCategorySelectedListener;

	public void setmOnCategorySelectedListener(
			OnCategorySelectedListener mOnCategorySelectedListener) {
		this.mOnCategorySelectedListener = mOnCategorySelectedListener;
	}

	public static interface OnCategorySelectedListener {
		public void onCategorySelected(Category cat);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (arg0 == null)
			return;
		Category cat = (Category) arg0.getItemAtPosition(arg2);
		if (cat != null && mOnCategorySelectedListener != null) {
			mOnCategorySelectedListener.onCategorySelected(cat);
		}

	}
}