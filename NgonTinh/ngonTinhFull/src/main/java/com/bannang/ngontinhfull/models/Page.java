package com.bannang.ngontinhfull.models;

public class Page {
	public String content = "";
	public int iStart;
	public int iEnd;
	public String startWord ="";
	public Page(String _content, int _iStart, int _iEnd) {
		content = _content;
		iStart = _iStart;
		iEnd = _iEnd;
	}
}
