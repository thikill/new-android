package com.bannang.ngontinhfull.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bannang.ngontinhfull.R;

@SuppressLint("NewApi")
public class ImageLoader {

	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;

	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ThreadPoolExecutor executorService;
	Handler handler = new Handler();// handler to display images in UI thread
	final int noImageResId, notAvailableImageResId;
	final int REQUIRED_SIZE = 256;

	@SuppressLint("NewApi")
	public ImageLoader(Context context, boolean sdcard, String folderName,
			int noImageResId, int notAvailableImageResId) {
		fileCache = new FileCache(context, sdcard, folderName);
		initExecutor();
		this.noImageResId = noImageResId;
		this.notAvailableImageResId = notAvailableImageResId;
	}

	private void initExecutor() {
		// executorService = Executors.newFixedThreadPool(5);
		executorService = new ThreadPoolExecutor(4, 16, 30, TimeUnit.SECONDS,
				new LifoBlockingDeque<Runnable>());
		if (Build.VERSION.SDK_INT >= 9) {
			executorService.allowCoreThreadTimeOut(true);
		}
	}

	public ImageLoader(Context context, boolean sdcard, String folderName) {
		this(context, sdcard, folderName, R.drawable.td_no_image,
				R.drawable.td_not_available);
	}

	public void displayImage(String url, ImageView imageView) {
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null) {
			if (imageView == null)
				return;
			imageViews.put(imageView, url);
			imageView.setImageBitmap(bitmap);
		} else {
			if (imageView != null) {
				imageView.clearAnimation();
				imageViews.put(imageView, url);
				imageView.setImageResource(noImageResId);
			}
			queuePhoto(url, imageView);
		}
	}

	public void removeImCache(String url) {
		Bitmap b = memoryCache.remove(url);
		Log.e("bitmap", "b=" + b);
		File f = fileCache.getFile(url);
		if (f != null)
			f.delete();
	}

	/** if imageView never used after that */
	public void displayImage1time(String url, ImageView imageView) {
		Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
			imageView.setVisibility(View.VISIBLE);
		} else {
			imageView.clearAnimation();
			imageView.setImageResource(noImageResId);
			queuePhoto(url, imageView);
		}
	}

	public void removeImageView(ImageView im) {
		imageViews.remove(im);
	}

	public void resetListView() {
		imageViews.clear();
	}

	public void reset() {
		imageViews.clear();
		executorService.shutdown();
		initExecutor();
	}

	private void queuePhoto(String url, ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.execute(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// from web
		try {
			Util.download2file(url, f);
			return decodeFile(f);
		} catch (Throwable ex) {
			ex.printStackTrace();
			if (ex instanceof OutOfMemoryError)
				memoryCache.clear();
			return null;
		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();

			// Find the correct scale value. It should be the power of 2.
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if ((width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE))
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;// */
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				if (photoToLoad.imageView == null) {
					File f = fileCache.getFile(photoToLoad.url);
					if (f.exists())
						return;
					Util.download2file(photoToLoad.url, f);
				} else {
					Bitmap bmp = getBitmap(photoToLoad.url);
					memoryCache.put(photoToLoad.url, bmp);
					if (imageViewReused(photoToLoad))
						return;
					BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
					handler.post(bd);
				}
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof PhotosLoader) {
				return photoToLoad.url
						.equals(((PhotosLoader) o).photoToLoad.url);
			}
			return super.equals(o);
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		if (photoToLoad.imageView == null)
			return false;
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null) {
				photoToLoad.imageView.setImageBitmap(bitmap);
				photoToLoad.imageView.startAnimation(AnimationUtils
						.loadAnimation(photoToLoad.imageView.getContext(),
								R.anim.td_load_img));
			} else {
				try {
					photoToLoad.imageView
							.setImageResource(notAvailableImageResId);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear(0);
	}

	public void clearCache(long lastModifiedTimeToNowMillis) {
		memoryCache.clear();
		fileCache.clear(lastModifiedTimeToNowMillis);
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

}
