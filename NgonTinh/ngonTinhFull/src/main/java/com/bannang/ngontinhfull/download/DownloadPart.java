package com.bannang.ngontinhfull.download;


public class DownloadPart {
	static String TempFolder = "/sdcard/.Temp";
	public long bytes;
	public long downloaded;
	public String filepath;
	public DownloadChildThread thread;
	public int idPart;
	public long start,end;
	public DownloadInit dl;
	public int state;
	public String lastMod;
	
	public DownloadPart(DownloadInit dl,int idPart,long start,long end){
		this.dl = dl;
		this.idPart = idPart;
		this.start = start;this.end = end;
		bytes = end - start;
		downloaded = 0;
		this.state = DownloadInit.STATUS_PENDING;
		filepath = TempFolder+"/"+dl.idDownload+"part"+idPart;
	}
	public static void setTempFolder(String s){
		TempFolder = s;
	}
}
