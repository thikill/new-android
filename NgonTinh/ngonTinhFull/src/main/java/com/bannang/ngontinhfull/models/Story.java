package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DatabaseField;

public class Story implements Comparable<Story>{
	@XmlTag("id")
	@DatabaseField(generatedId = true)
	public Integer id;
	@XmlTag("name")
	@DatabaseField
	public String name;
	
	@DatabaseField
	public String description;

	@XmlTag("category_id")
	@DatabaseField(columnName = "category_id")
	public Integer categoryId;
	@DatabaseField
	public String author;

	
	@Override
	public int compareTo(Story another) {
		return Constants.COLLATOR.getCollationKey(this.name).compareTo(
				Constants.COLLATOR.getCollationKey(another.name));
	}	
}
