package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.CSUtil;
import com.bannang.ngontinhfull.common.XmlTag;

public class StartAppConfig {

	@XmlTag("startAppEnable_1")
	public String startAppEnable_1 = "0";

	public boolean getStartAppEnable1() {
		return !"0".equals(startAppEnable_1);
	}

	@XmlTag("startAppEnable_2")
	public String startAppEnable_2 = "0";

	public boolean getStartAppEnable2() {
		return !"0".equals(startAppEnable_2);
	}

	@XmlTag("startAppEnable_3")
	public String startAppEnable_3 = "0";

	public boolean getStartAppEnable3() {
		return !"0".equals(startAppEnable_3);
	}

	@XmlTag("startAppEnable_4")
	public String startAppEnable_4 = "0";

	public boolean getStartAppEnable4() {
		return !"0".equals(startAppEnable_4);
	}

	@XmlTag("startAppEnable_5")
	public String startAppEnable_5 = "0";

	public boolean getStartAppEnable5() {
		return !"0".equals(startAppEnable_5);
	}

	@XmlTag("startAppEnable_6")
	public String startAppEnable_6 = "0";

	public boolean getStartAppEnable6() {
		return !"0".equals(startAppEnable_6);
	}

	@XmlTag("startAppEnable_7")
	public String startAppEnable_7 = "0";

	public boolean getStartAppEnable7() {
		return !"0".equals(startAppEnable_7);
	}

	@XmlTag("startAppEnable_8")
	public String startAppEnable_8 = "0";

	public boolean getStartAppEnable8() {
		return !"0".equals(startAppEnable_8);
	}

	@XmlTag("startAppEnable_9")
	public String startAppEnable_9 = "0";

	public boolean getStartAppEnable9() {
		return !"0".equals(startAppEnable_9);
	}

	@XmlTag("startAppEnable_10")
	public String startAppEnable_10 = "0";

	public boolean getStartAppEnable10() {
		return !"0".equals(startAppEnable_10);
	}

	public String toString() {
		try {
			return CSUtil.Object2JSonString(this, null);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}