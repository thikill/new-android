package com.bannang.ngontinhfull.views;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.models.Category;

public class StoryViewItem extends BaseLinearLayout {
	private ImageView mThumbView;
	private TextView mLabel;

	public Category mCat;

	public StoryViewItem(Context context) {
		super(context, R.layout.category_view_item);

		mThumbView = (ImageView) findViewById(R.id.imageThumb);
		mLabel = (TextView) findViewById(R.id.tvLabel);
	}

	public void setData(Category cat) {
		mCat = cat;
		mLabel.setText(cat.name);
		Static.getIL(mContext).displayImage(
				"https://dl.dropboxusercontent.com/u/223793945/story/"
						+ cat.firstBookId, mThumbView);

	}

}
