package com.bannang.ngontinhfull.common;

import android.content.Context;

/**
 * Helps me to do some quick access in context/ui thread
 * 
 * @author thimb
 * 
 */
public class Contexts {
	public Context appContext;

	String pref_password = "";

	private static Contexts instance;

	

	/** get a Contexts instance for activity use **/
	static public Contexts instance() {
		if (instance == null) {
			synchronized (Contexts.class) {
				if (instance == null) {
					instance = new Contexts();
				}
			}
		}
		return instance;
	}

	synchronized boolean initApplication(Object appInitialObject,
			Context context) {
		if (appContext == null) {
			appContext = context.getApplicationContext();
			return true;
		}
		return false;
	}


	
}
