package com.bannang.ngontinhfull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.Logger;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.controllers.Controller;
import com.bannang.ngontinhfull.models.Chapter;
import com.bannang.ngontinhfull.models.Download;
import com.bannang.ngontinhfull.models.DownloadItem;
import com.bannang.ngontinhfull.models.Favorite;
import com.bannang.ngontinhfull.models.History;
import com.bannang.ngontinhfull.models.Page;
import com.bannang.ngontinhfull.models.Story;
import com.bannang.ngontinhfull.views.MenuView;
import com.bannang.ngontinhfull.views.ReaderContentView;
import com.bannang.ngontinhfull.views.MenuView.OnChapterSelectedListener;
import com.bannang.ngontinhfull.views.ReaderContentView.ReaderInterface;
import com.bannang.ngontinhfull.views.dialog.ChooseFontDialog;
import com.bannang.ngontinhfull.views.dialog.CommentDialog;
import com.bannang.ngontinhfull.views.dialog.ChooseFontDialog.ChooseFontInterface;
import com.j256.ormlite.dao.Dao;
import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;

public class ReaderActivity extends SherlockFragmentActivity implements
		OnChapterSelectedListener, ReaderInterface, OnNavigationListener,
		ChooseFontInterface {
	public static final String KEY_CATEGORY = "KEY_CATEGORY";
	public static final String KEY_STORY = "KEY_STORY";
	public static final String KEY_STORY_CHAP = "KEY_STORY_CHAP";
	public static final String KEY_STORY_START = "KEY_STORY_START";

	protected MenuView mMenu;
	protected ReaderContentView mReaderContent;
	private ProgressBar mLoading;
	private LinearLayout mMainContainer;
	private Story mStory;
	private List<Chapter> mChapters;
	private Chapter mCurrentChap = null;
	private Bundle mSavedInstanceSate = null;
	public static ReaderActivity INSTANCE;
	public boolean isChapterList = false;
	private History mHistory;
	private Integer storyId;
	public Integer chapId;
	public Integer catId;
	private StartAppAd startAppAd = new StartAppAd(this);
	private Handler mHandler;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startAppAd = new StartAppAd(this);
		startAppAd.loadAd();
		mHandler = new Handler();
		INSTANCE = this;
		setContentView(R.layout.listen_content_bar);
		if (Static.getInfo().getStartAppEnable1()) {
			StartAppSearch.showSearchBox(this);
		}
		mMainContainer = (LinearLayout) findViewById(R.id.linearMainContent);
		mMenu = new MenuView(this);
		mMenu.setListener(this);
		mReaderContent = new ReaderContentView(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
		if (Static.TRACKER != null)
			Static.TRACKER.sendView("ReaderActivity");
		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			isChapterList = true;
			Bundle data = intent.getExtras();
			catId = data.getInt(KEY_CATEGORY, 0);
			storyId = data.getInt(KEY_STORY, 0);
			chapId = data.getInt(KEY_STORY_CHAP, 0);
			Integer startIndex = data.getInt(KEY_STORY_START, 0);
			if (Build.VERSION.SDK_INT >= 11) {	invalidateOptionsMenu();}
			loadStoryFromServer(storyId, chapId, startIndex);
		}
	}
	private Runnable mCloseAdd = new Runnable() {

		@Override
		public void run() {
			startAppAd.close();
		}
	};
	
	@Override
	public void onResume() {
		super.onResume();
		startAppAd.onResume();
	}


	@Override
	public void onPause() {
		super.onPause();
		startAppAd.onPause();
	}
	public MenuView setMainMenu() {
		mMainContainer.removeAllViews();
		mMenu.startAnimation(Static.getShowViewAnimation(this));
		mMainContainer.addView(mMenu);
		return mMenu;
	}

	private boolean mbFirst = false;

	@Override
	public void onChapterSelected(Chapter chap) {
		// mLinearFont.setVisibility(View.GONE);
		isChapterList = false;
		if (Build.VERSION.SDK_INT >= 11) {	invalidateOptionsMenu();}
		mCurrentChap = chap;
		mReaderContent.setData(chap, catId);

		mReaderContent.setReaderInterface(this);
		mMainContainer.removeAllViews();
		mMainContainer.addView(mReaderContent);

		if (mList != null && !mbFirst) {
			mbFirst = true;
			getSupportActionBar().setNavigationMode(
					ActionBar.NAVIGATION_MODE_LIST);
			getSupportActionBar().setListNavigationCallbacks(mList,
					ReaderActivity.this);

		}
		int index = mChapters.indexOf(chap);
		if (index > 0)
			getSupportActionBar().setSelectedNavigationItem(index);

		// update last chapter

		try {
			Dao<History, Integer> historyDao = Static.getDB(this)
					.getHistoryDao();
			mHistory = new History();
			mHistory.bookId = storyId;
			mHistory.curChapId = chap.id;
			historyDao.createOrUpdate(mHistory);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ArrayAdapter<Chapter> mList;

	private void loadStoryFromServer(final Integer storyId,
			final Integer chapId, final Integer startIndex) {
		if (Static.TRACKER != null)
			Static.TRACKER.sendEvent("loadStory", "storyId =" + storyId, "",
					null);
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Chapter>> task = new AsyncTask<Void, Void, List<Chapter>>() {
			private Exception mEx = null;

			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Chapter> doInBackground(Void... params) {
				try {

					Dao<History, Integer> historyDao = Static.getDB(
							ReaderActivity.this).getHistoryDao();
					mHistory = historyDao.queryForFirst(historyDao
							.queryBuilder().where().eq("book_id", storyId)
							.prepare());

					Dao<Story, Integer> storyDao = Static.getDB(
							ReaderActivity.this).getStoryDao();
					mStory = storyDao.queryForId(storyId);
					if (mStory == null)
						return null;
					Dao<Chapter, Integer> chapterDao = Static.getDB(
							ReaderActivity.this).getChapterDao();
					List<Chapter> chapters = chapterDao.query(chapterDao
							.queryBuilder().where().eq("story_id", storyId)
							.prepare());
					if (chapters == null)
						return null;
					for (Chapter chap : chapters) {
						if (chap.content == null || chap.content.length == 1) {
							File file = new File(Environment
									.getExternalStorageDirectory()
									.getAbsoluteFile()
									+ File.separator
									+ Constants.DOWNLOAD_FOLDER
									+ File.separator + chap.id);
							if (file.exists() && file.length() > 10) {
								FileInputStream fis = new FileInputStream(file);
								chap.content = new byte[(int) file.length()];
								fis.read(chap.content);
								chap.content = Static.decompress(chap.content);
							}
						} else {
							chap.content = Static.decompress(chap.content);
						}
					}
					return chapters;
				} catch (Exception e) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(List<Chapter> result) {
				mLoading.setVisibility(View.GONE);
				if (mEx != null) {
					onNetworkError(mEx);
				}
				if (result == null || result.size() == 0)
					return;
				mChapters = result;
				if (mChapters.size() == 1) {
					mCurrentChap = result.get(0);
					readFromLastBookmark(null, startIndex);
					onChapterSelected(mCurrentChap);
				} else {

					mList = new ArrayAdapter<Chapter>(ReaderActivity.this,
							android.R.layout.simple_dropdown_item_1line,
							mChapters) {
						public android.view.View getView(int position,
								android.view.View convertView,
								android.view.ViewGroup parent) {
							TextView tv = (TextView) convertView;
							if (tv == null) {
								LayoutInflater lif = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								tv = (TextView) lif
										.inflate(
												android.R.layout.simple_dropdown_item_1line,
												null);

							}
							tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
							tv.setPadding(10, 10, 10, 10);
							if (getItem(position).name != null
									&& getItem(position).name.length() > 0) {
								tv.setText(Html
										.fromHtml(getItem(position).name));
							} else {
								tv.setText("Chapter " + (position++));
							}
							return tv;
						};

						public android.view.View getDropDownView(int position,
								android.view.View convertView,
								android.view.ViewGroup parent) {

							TextView tv = (TextView) convertView;
							if (tv == null) {
								LayoutInflater lif = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								tv = (TextView) lif
										.inflate(
												android.R.layout.simple_dropdown_item_1line,
												null);
							}
							tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
							tv.setPadding(10, 10, 10, 10);
							if (getItem(position).name != null
									&& getItem(position).name.length() > 0) {
								tv.setText(Html
										.fromHtml(getItem(position).name));
							} else {
								tv.setText("Chapter " + (position++));
							}
							return tv;
						};
					};
					setMainMenu().setData(result);
					if (mStory != null)
						Static.TRACKER.sendEvent("Load Story", "Story name = "
								+ mStory.name, "", null);
					if (chapId >= 0 && chapId < result.size()
							&& result.get(chapId) != null) {
						readFromLastBookmark(result.get(chapId), startIndex);
					}
				}
			}

		};
		Utils.executeAsyncTask(task);
	}

	private void loadStory(final Integer storyId, final Integer chapId,
			final Integer startIndex) {
		if (Static.TRACKER != null)
			Static.TRACKER.sendEvent("loadStory", "storyId =" + storyId, "",
					null);
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Chapter>> task = new AsyncTask<Void, Void, List<Chapter>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
				super.onPreExecute();
			}

			@Override
			protected List<Chapter> doInBackground(Void... params) {
				try {
					Dao<Story, Integer> storyDao = Static.getDB(
							ReaderActivity.this).getStoryDao();
					mStory = storyDao.queryForId(storyId);
					if (mStory == null)
						return null;
					//
					//
					// for (Chapter chap : chapters) {
					// if (chap.content == null) {
					// File file = new File(Environment
					// .getExternalStorageDirectory()
					// .getAbsoluteFile()
					// + "/dt/" + chap.id);
					// if (file.exists() && file.length() > 10) {
					// FileInputStream fis = new FileInputStream(file);
					// chap.content = new byte[(int) file.length()];
					// fis.read(chap.content);
					// chap.content = Static.decompress(chap.content);
					// fis.close();
					// }
					// } else {
					// chap.content = Static.decompress(chap.content);
					// }
					// }
					// return chapters;
				} catch (Exception e) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(List<Chapter> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0)
					return;
				mChapters = result;
				if (mChapters.size() == 1) {
					mCurrentChap = result.get(0);
					readFromLastBookmark(null, startIndex);
					onChapterSelected(mCurrentChap);
				} else {

					mList = new ArrayAdapter<Chapter>(ReaderActivity.this,
							android.R.layout.simple_dropdown_item_1line,
							mChapters) {
						public android.view.View getView(int position,
								android.view.View convertView,
								android.view.ViewGroup parent) {
							TextView tv = (TextView) convertView;
							if (tv == null) {
								LayoutInflater lif = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								tv = (TextView) lif
										.inflate(
												android.R.layout.simple_dropdown_item_1line,
												null);

							}
							tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
							tv.setPadding(10, 10, 10, 10);
							if (getItem(position).name != null
									&& getItem(position).name.length() > 0) {
								tv.setText(Html
										.fromHtml(getItem(position).name));
							} else {
								tv.setText("Chapter " + (position++));
							}
							return tv;
						};

						public android.view.View getDropDownView(int position,
								android.view.View convertView,
								android.view.ViewGroup parent) {

							TextView tv = (TextView) convertView;
							if (tv == null) {
								LayoutInflater lif = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								tv = (TextView) lif
										.inflate(
												android.R.layout.simple_dropdown_item_1line,
												null);
							}
							tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
							tv.setPadding(10, 10, 10, 10);
							if (getItem(position).name != null
									&& getItem(position).name.length() > 0) {
								tv.setText(Html
										.fromHtml(getItem(position).name));
							} else {
								tv.setText("Chapter " + (position++));
							}
							return tv;
						};
					};
					setMainMenu().setData(result);
					if (mStory != null)
						Static.TRACKER.sendEvent("Load Story", "Story name = "
								+ mStory.name, "", null);
					if (chapId >= 0 && chapId < result.size()
							&& result.get(chapId) != null) {
						readFromLastBookmark(result.get(chapId), startIndex);
					}
				}
			}

		};
		Utils.executeAsyncTask(task);
	}

	private void readFromLastBookmark(final Chapter chap, final int startIndex) {
		if (startIndex == 0) {
			return;
		}
		new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_app)
				.setTitle(R.string.bookmark_page)
				.setMessage(R.string.bookmark_page_msg)
				.setPositiveButton(R.string.Application_ConfirmExitOK,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Static.currentStartPage = new Page("",
										startIndex, startIndex);
								if (chap != null) {
									onChapterSelected(chap);
								}
							}
						})
				.setNegativeButton(R.string.Application_ConfirmExitCancel, null)
				.show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (isChapterList == false) {
			getSupportMenuInflater().inflate(R.menu.reader_menu, menu);
		} else {
			getSupportMenuInflater().inflate(R.menu.chapter_list, menu);
		}

		return true;
	}

	@Override
	public void readLast() {
		// if (mMenu.mChapters != null && mMenu.mChapters.size() > 1) {
		// setMainMenu();
		// }
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
			
		case R.id.favorite:

			try {
				Dao<Favorite, Integer> daoFavorite = Static.getDB(
						ReaderActivity.this).getFavoriteDao();
				Favorite favorite = new Favorite();
				favorite.bookId = storyId;
				//daoFavorite.deleteById(storyId);
				daoFavorite.createOrUpdate(favorite);
				Toast.makeText(this,
						getString(R.string.bookmark_story_success),
						Toast.LENGTH_SHORT).show();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case R.id.download:
			try {
				if (Utils.checkNetworkStatus(ReaderActivity.this) == true) {
					Toast.makeText(this, getString(R.string.download_story_begin),
							Toast.LENGTH_SHORT).show();
					String mUrl=null;
					//mUrl = "https://dl.dropboxusercontent.com/u/223793945/book/"
							//+ storyId;
					//doDownloadStart(mUrl, null, null, null, 0);
					for (Chapter chap : mChapters) {
						mUrl = "https://dl.dropboxusercontent.com/u/223793945/chapter/"
								+ chap.id;
						doDownloadStart(mUrl, null, null, null, 0);
					}

					Dao<Download, Integer> daoDownload = Static.getDB(
							ReaderActivity.this).getDownloadDao();
					Download downloadInfo = new Download();
					downloadInfo.bookId = storyId;
					daoDownload.createOrUpdate(downloadInfo);

					Toast.makeText(this,
							getString(R.string.download_story_succes),
							Toast.LENGTH_SHORT).show();
				}
			} catch (SQLException e) {
				Toast.makeText(this, R.string.network_err, Toast.LENGTH_SHORT)
						.show();
				e.printStackTrace();
			}
			return true;			
		case R.id.setting_font:
			ChooseFontDialog cfd = new ChooseFontDialog(this);
			cfd.setInterface(this);
			cfd.show();
			return true;
			/*
			 * case R.id.comment: if (mCurrentChap == null) return true;
			 * CommentDialog od = new CommentDialog(this, mCurrentChap.id);
			 * od.show(); return true;
			 */
		}
		return super.onOptionsItemSelected(item);
	}
	private void doDownloadStart(String url, String userAgent,
			String contentDisposition, String mimetype, long contentLength) {

		if (Utils.checkCardState(this, true)) {
			DownloadItem item = new DownloadItem(this, url);
			Controller.getInstance().addToDownload(item);
			item.startDownload();

		}
	}
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if (mChapters == null || itemPosition >= mChapters.size()
				|| mCurrentChap == null)
			return true;
		if (mCurrentChap == mChapters.get(itemPosition)) {
			return true;

		}
		Static.currentStartPage = null;
		onChapterSelected(mChapters.get(itemPosition));
		return true;
	}

	@Override
	public void onChooseFont(float font) {
		Static.textSize = font;
		if (mCurrentChap != null)
			onChapterSelected(mCurrentChap);
	}

	@Override
	public void onNetworkError(Exception e) {
		setMainMenu();
		mMenu.findViewById(R.id.networkError).setVisibility(View.VISIBLE);
	}

	@Override
	public void finish() {
		if (mStory != null) {
			try {
				Dao<History, Integer> historyDao = Static.getDB(this)
						.getHistoryDao();
				History history = historyDao.queryForFirst(historyDao
						.queryBuilder().where().eq("book_id", mStory.id)
						.prepare());

				if (history == null)
					history = new History();

				history.bookId = mStory.id;
				history.readTime = new Date(System.currentTimeMillis());
				if (mCurrentChap != null) {
					history.curChapId = mCurrentChap.id;
				}
				if (Static.currentStartPage != null)
					history.curCharIndex = Static.currentStartPage.iStart;

				historyDao.createOrUpdate(history);

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		super.finish();
	}
	
	@Override
	public void onBackPressed() {
		if (mMainContainer.getChildAt(0) == mMenu || mMenu.mChapters == null
				|| mMenu.mChapters != null && mMenu.mChapters.size() == 1) {
			showAdd(Static.getInfo().getStartAppEnable5());
			super.onBackPressed();
		} else {
			showAdd(Static.getInfo().getStartAppEnable7());
			setMainMenu();
		}
	}
	
	private void showAdd(boolean bShow) {
		if(!bShow) return;

		startAppAd.showAd();
		startAppAd.loadAd();
		mHandler.postDelayed(mCloseAdd, 10000);
	}
}
