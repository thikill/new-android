package com.bannang.ngontinhfull;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.util.Log;

public class ZipDecompress {
	private InputStream _zipFile;
	private String _location;

	public ZipDecompress(InputStream zipFile, String location)
			throws FileNotFoundException {
		_zipFile = zipFile;
		_location = location;

		_dirChecker("");
	}

	public void unzip() {
		try {
			ZipInputStream zin = new ZipInputStream(_zipFile);
			ZipEntry entry = null;
			while ((entry = zin.getNextEntry()) != null) {
				Log.v("Decompress", "Unzipping " + entry.getName());

				if (entry.isDirectory()) {
					_dirChecker(entry.getName());
				} else {
					int size;
					byte[] buffer = new byte[2048];

					FileOutputStream outStream = new FileOutputStream(_location
							+ entry.getName());
					BufferedOutputStream bufferOut = new BufferedOutputStream(
							outStream, buffer.length);

					while ((size = zin.read(buffer, 0, buffer.length)) != -1) {
						bufferOut.write(buffer, 0, size);
					}

					bufferOut.flush();
					bufferOut.close();
				}

			}
			zin.close();
			_zipFile.close();
		} catch (Exception e) {
			Log.e("Decompress", "unzip", e);
		}

	}

	public void unzip(String filePath) {
		try {
			ZipInputStream zin = new ZipInputStream(_zipFile);
			ZipEntry entry = null;
			while ((entry = zin.getNextEntry()) != null) {
				Log.v("Decompress", "Unzipping " + entry.getName());

				if (entry.isDirectory()) {
					_dirChecker(entry.getName());
				} else {
					int size;
					byte[] buffer = new byte[2048];

					FileOutputStream outStream = new FileOutputStream(filePath);
					BufferedOutputStream bufferOut = new BufferedOutputStream(
							outStream, buffer.length);

					while ((size = zin.read(buffer, 0, buffer.length)) != -1) {
						bufferOut.write(buffer, 0, size);
					}

					bufferOut.flush();
					bufferOut.close();
				}

			}
			zin.close();
			_zipFile.close();
		} catch (Exception e) {
			Log.e("Decompress", "unzip", e);
		}

	}

	private void _dirChecker(String dir) {
		File f = new File(_location + dir);
		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}
}