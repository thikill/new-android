package com.bannang.ngontinhfull.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Comment;
import com.bannang.ngontinhfull.models.User;
import com.bannang.ngontinhfull.request.CommentLoadRequest;
import com.bannang.ngontinhfull.request.UserRegisterRequest;
import com.bannang.ngontinhfull.response.BaseResponse;
import com.bannang.ngontinhfull.response.CommentLoadResponse;
import com.bannang.ngontinhfull.views.CommentItemView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.TdPullToCloseScrollView;

public class CommentDialog extends Dialog implements
		OnRefreshListener2<ScrollView>, android.view.View.OnClickListener {
	private ScrollView mScrollView;
	private TdPullToCloseScrollView mP2rScrollView, mP2rScrollView2;
	private Context mContext;
	private ImageView btnClose;
	private ProgressBar mProgress;

	private ListView mListComment;
	private CommentAdapter mAdapter;
	private Button mBtnUserRegister;
	private Button mBtnCommentSubmit;
	private int mChapId;

	public CommentDialog(Context context, int chapId) {
		super(context, R.style.FB_Comment_Dialog);
		mContext = context;
		mChapId = chapId;
		setContentView(R.layout.layout_option_dialog);
		getWindow().getAttributes().width = LayoutParams.MATCH_PARENT;
		getWindow().getAttributes().height = LayoutParams.MATCH_PARENT;
		getWindow().setWindowAnimations(R.style.FbDialogAni_from_bottom2);

		mP2rScrollView = (TdPullToCloseScrollView) findViewById(R.id.pullToRefreshScrollView);
		// mP2rScrollView2 = (TdPullToCloseScrollView)
		// findViewById(R.id.pullToRefreshScrollView2);

		mP2rScrollView.setOnRefreshListener(this);
		mP2rScrollView.setMode(Mode.BOTH);
		mScrollView = mP2rScrollView.getRefreshableView();
		mScrollView.setBackgroundColor(Color.WHITE);

		// mP2rScrollView2.setOnRefreshListener(this);
		// mP2rScrollView2.setMode(Mode.BOTH);
		// mP2rScrollView2.getRefreshableView().setBackgroundColor(Color.WHITE);

		btnClose = (ImageView) findViewById(R.id.btn_close);
		btnClose.setOnClickListener(this);

		mProgress = (ProgressBar) findViewById(R.id.progressLoadComment);
		mListComment = (ListView) findViewById(R.id.listComment);

		mBtnCommentSubmit = (Button) findViewById(R.id.btnSubmitComment);
		mBtnUserRegister = (Button) findViewById(R.id.btnUserRegister);

		mAdapter = new CommentAdapter(mContext);
		mListComment.setAdapter(mAdapter);

		mBtnUserRegister.setOnClickListener(this);
		mBtnCommentSubmit.setOnClickListener(this);
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
		Log.i("PULL", "PULLDOWN");
		getWindow().setWindowAnimations(R.style.FbDialogAni_from_top2);
		dismiss();
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
		Log.i("PULL", "PULLUP");
		getWindow().setWindowAnimations(R.style.FbDialogAni_from_bottom2);
		dismiss();
	}

	@Override
	public void onClick(View v) {
		if (v == btnClose) {
			dismiss();
			return;
		}
		if (v == mBtnCommentSubmit) {
			userRegister();
			return;
		}
		if (v == mBtnUserRegister) {
			commentSubmit();
			return;
		}
	}

	@Override
	public void show() {
		loadComment();

		super.show();
	}

	private void userRegister() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {

				try {

					UserRegisterRequest urr = new UserRegisterRequest();
					urr.user = new User();
					urr.user.name = "Test name";
					urr.user.deviceId = Static.getDeviceId(mContext);

					BaseResponse br = (BaseResponse) urr.postRequest(null);
					if (br.rawData != null)
						br.rawData.close();

				} catch (Exception e) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
			}

		};
		Utils.executeAsyncTask(task);
	}

	private void commentSubmit() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return null;
			}

		};
		Utils.executeAsyncTask(task);
	}

	private void loadComment() {
		AsyncTask<Void, Void, CommentLoadResponse> task = new AsyncTask<Void, Void, CommentLoadResponse>() {
			@Override
			protected void onPreExecute() {
				mProgress.setVisibility(View.VISIBLE);
			}

			@Override
			protected CommentLoadResponse doInBackground(Void... params) {
				try {
					CommentLoadRequest clr = new CommentLoadRequest();
					clr.comment = new Comment();
					clr.comment.chapterid = mChapId;
					return (CommentLoadResponse) clr
							.postRequest(CommentLoadResponse.class);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(CommentLoadResponse result) {
				mProgress.setVisibility(View.GONE);
			}

		};
		Utils.executeAsyncTask(task);
	}

	public static class CommentAdapter extends ArrayAdapter<String> {
		private Context mContext;

		public CommentAdapter(Context context) {
			super(context, R.layout.comment_item);
			mContext = context;
		}

		@Override
		public int getCount() {
			return 30;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return new CommentItemView(mContext);
		}

	};
}
