package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Comment;

public class CommentSaveRequest extends BaseRequest {
	@XmlTag("comment")
	public Comment comment;

	public CommentSaveRequest() {
		super(getUrl("comment/save"));
	}

}
