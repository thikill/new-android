package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Story;

public class UploadStoriesRequest extends BaseRequest {
	@XmlTag("stories")
	public Story[] stories; 
	public UploadStoriesRequest() {
		super(getUrl("story/setstories"));
	}

}
