package com.bannang.ngontinhfull.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.common.CSUtil;

public class LoginDialog extends Dialog implements
		android.view.View.OnClickListener {

	private Button mBtnCancel;
	private Button mBtnLogin;

	private EditText mEditPassword;
	private TextView mTvErrorMessage;

	public LoginDialog(Context context) {
		super(context);
		setContentView(R.layout.c_login_dlg);
		setCancelable(true);
		setCanceledOnTouchOutside(false);

		mBtnCancel = (Button) findViewById(R.id.btnCancel);
		mBtnLogin = (Button) findViewById(R.id.btnLogin);

		mEditPassword = (EditText) findViewById(R.id.editPassword);
		mTvErrorMessage = (TextView) findViewById(R.id.tvError);

		mBtnCancel.setOnClickListener(this);
		mBtnLogin.setOnClickListener(this);
		setTitle(R.string.login);

	}

	@Override
	public void onClick(View v) {
		InputMethodManager imm = (InputMethodManager) getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditPassword.getWindowToken(), 0);

		if (mLoginInterface == null)
			return;
		if (v == mBtnCancel) {
			dismiss();
			mLoginInterface.cancel();
			return;
		}
		if (v == mBtnLogin) {
			String password = CSUtil.getPrefPassword(getContext());
			if (password.equals(mEditPassword.getText().toString())) {
				dismiss();
				mLoginInterface.success();
				mTvErrorMessage.setVisibility(View.GONE);
			} else {
				mTvErrorMessage.setVisibility(View.VISIBLE);
			}
		}
	}

	private LoginInterface mLoginInterface;

	public void setmLoginInterface(LoginInterface mLoginInterface) {
		this.mLoginInterface = mLoginInterface;
	}

	public static interface LoginInterface {
		public void cancel();

		public void success();
	}

}
