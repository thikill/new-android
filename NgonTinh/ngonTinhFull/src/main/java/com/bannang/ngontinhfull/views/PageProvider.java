package com.bannang.ngontinhfull.views;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.models.Page;
import com.bannang.ngontinhfull.views.pagecurl.CurlPage;

class PageProvider implements com.bannang.ngontinhfull.views.pagecurl.CurlView.PageProvider {
	private List<Page> mPages;
	private TextView mTvContent;
	private Context mContext;
	private Paint p;

	public PageProvider(Context context, List<Page> pages) {
		mPages = pages;
		mContext = context;
		mTvContent = new TextView(context);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				-1, -1);
		mTvContent.setLayoutParams(layoutParams);
		mTvContent.setText("");
		mTvContent.setTextSize(TypedValue.COMPLEX_UNIT_DIP, Static.textSize);
		mTvContent.setTextColor(Color.BLACK);
		mTvContent.setPadding(40, 20, 20, 20);
		mTvContent.setBackgroundColor(Color.TRANSPARENT);
		p = new Paint();
		p.setStyle(Style.STROKE);
		p.setColor(Color.BLUE);
		p.setStrokeWidth(1.0f);
		p.setTextAlign(Align.CENTER);
		p.setTextSize(33.0f);
		p.setFlags(Paint.ANTI_ALIAS_FLAG|Paint.DITHER_FLAG);
	}

	@Override
	public int getPageCount() {
		return mPages.size();
	}

	private Bitmap loadBitmap(int width, int heigh, int index) {
		Bitmap b = Bitmap.createBitmap(width, heigh, Bitmap.Config.ARGB_8888);
		b.eraseColor(0xFFFFFFFF);
		Canvas c = new Canvas(b);

		mTvContent.layout(0, 0, width, heigh);
		mTvContent.setText(mPages.get(index).content);
		mTvContent.draw(c);

		Rect bounds = new Rect();
		p.setTextAlign(Align.CENTER);
		String gText = "Trang " + (index + 1) + "/" + mPages.size();

		p.getTextBounds(gText, 0, gText.length(), bounds);
		int x = (width + 40 - bounds.width()) / 2;
		int y = heigh - 20 - bounds.height() / 2;

		c.drawText(gText, x, y, p);
		Static.currentStartPage = mPages.get(index);
		return b;
	}

	@Override
	public void updatePage(CurlPage page, int width, int height, int index) {
		
		Bitmap front = loadBitmap(width, height, index);
		page.setTexture(front, CurlPage.SIDE_FRONT);
		Bitmap back = BitmapFactory.decodeResource(mContext.getResources(),
				com.bannang.ngontinhfull.R.drawable.background);
		page.setTexture(back, CurlPage.SIDE_BACK);
		page.setColor(Color.rgb(255, 190, 150), CurlPage.SIDE_FRONT);
		page.setColor(Color.argb(127, 170, 130, 255), CurlPage.SIDE_BACK);



	}

}