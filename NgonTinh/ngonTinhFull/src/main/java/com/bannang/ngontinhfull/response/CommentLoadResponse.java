package com.bannang.ngontinhfull.response;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Comment;

public class CommentLoadResponse extends BaseResponse {
	@XmlTag("comments")
	public Comment[] comments;
}
