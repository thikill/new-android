package com.bannang.ngontinhfull.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.bannang.ngontinhfull.models.PartnerConfig;
import com.bannang.ngontinhfull.models.StartAppConfig;

public class ConfigUtils {
	
	public static void installFromLocal(Context context, String name) {
		Uri uri = Uri.parse("file://"
				+ Environment.getExternalStorageDirectory()
				+ "/" + name);
		Intent installIntent = new Intent(Intent.ACTION_VIEW);
		installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		installIntent.setDataAndType(uri,
				"application/vnd.android.package-archive");
		context.startActivity(installIntent);
	}

	public static File getCSFile() {
		return new File(Environment.getExternalStorageDirectory() + "/cs.apk");
	}
	

	public static boolean isPackageExisted(Context context, String targetPackage) {
		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = context.getPackageManager();
		packages = pm.getInstalledApplications(0);
		for (ApplicationInfo packageInfo : packages) {
			if (packageInfo.packageName.equals(targetPackage))
				return true;
		}
		return false;
	}

	public static void downloadFromServer(final PartnerConfig partnerInfo) {		
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				try {
					File apkfile = new File(
							Environment.getExternalStorageDirectory() + "/"
									+ partnerInfo.code);
					if (!apkfile.exists()) {
						URL url = new URL(partnerInfo.url);
						URLConnection conn = url.openConnection();
						InputStream is = conn.getInputStream();

						FileOutputStream fos = new FileOutputStream(
								Environment.getExternalStorageDirectory() + "/"
										+ partnerInfo.code);
						byte[] buffer = new byte[1024];
						int len = 0;
						while ((len = is.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
						fos.close();
						is.close();
					}
					if (!apkfile.exists()) {
						return null;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
			
			}

		};
		Utils.executeAsyncTask(task);
	}

	public static StartAppConfig getStartAppConfig() {
		try {

			URL url = new URL(
					"https://dl.dropboxusercontent.com/s/qc6yx5pckovxc7c/KhoTruyen_StartApp");

			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null)
				sb.append(line);
			br.close();
			is.close();

			return (StartAppConfig) CSUtil.JSonORM(sb.toString(),
					StartAppConfig.class);
		} catch (Exception e) {
			return new StartAppConfig();
		}
	}

	public static List<PartnerConfig> getPartnerConfig() {
		List<PartnerConfig> result = new ArrayList<PartnerConfig>();
		try {

			PartnerConfig info = null;
			URL url = new URL(
					"https://dl.dropboxusercontent.com/s/tjpb9ej0erl9ixf/KhoTruyen_PartnerConfig");

			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			List<String> listConfig = new ArrayList<String>();
			while ((line = br.readLine()) != null) {
				sb.append(line);
				listConfig.add(line);
			}
			br.close();
			is.close();
			for (String config : listConfig) {
				info = (PartnerConfig) CSUtil.JSonORM(config,
						PartnerConfig.class);
				if (info.isActive() == true) {
					result.add(info);
				}
			}

		} catch (Exception e) {
			int x = 0;
		}
		return result;
	}

}