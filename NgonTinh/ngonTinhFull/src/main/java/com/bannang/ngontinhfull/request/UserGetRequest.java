package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.User;

public class UserGetRequest extends BaseRequest {
	@XmlTag("user")
	public User user;

	public UserGetRequest() {
		super(getUrl("user/get"));
	}

}
