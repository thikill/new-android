package com.bannang.ngontinhfull.views;

import java.util.ArrayList;
import java.util.List;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.GUIs;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Download;
import com.bannang.ngontinhfull.models.History;
import com.bannang.ngontinhfull.models.Story;
import com.j256.ormlite.dao.Dao;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class DownloadView extends BaseLinearLayout {
	private ListView mListBook;
	private ProgressBar mLoading;

	public DownloadView(Context context) {
		super(context, R.layout.favorite_view);
		mListBook = (ListView) findViewById(R.id.listViewBooks);
		mLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
		loadList();
	}

	public void loadList() {
		Static.cancellAllTask();
		AsyncTask<Void, Void, List<Story>> task = new AsyncTask<Void, Void, List<Story>>() {
			@Override
			protected void onPreExecute() {
				mLoading.setVisibility(View.VISIBLE);
			}

			@Override
			protected List<Story> doInBackground(Void... params) {
				try {
					Dao<Download, Integer> downloadDao = Static.getDB(mContext)
							.getDownloadDao();
					List<Download> downloads = downloadDao.queryForAll();
					List<Integer> bookIds = new ArrayList<Integer>();

					for (Download download : downloads) {
						bookIds.add(download.bookId);
					}
					Dao<Story, Integer> storyDao = Static.getDB(mContext)
							.getStoryDao();
					return storyDao.query(storyDao.queryBuilder().where()
							.in("id", bookIds).prepare());

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(List<Story> result) {
				mLoading.setVisibility(View.GONE);
				if (result == null || result.size() == 0) {
					return;
				}
				loadList(result);
			}
		};
		Utils.executeAsyncTask(task);
	}

	public void loadList(List<Story> stories) {

		mListBook.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Story story = (Story) mListBook.getAdapter().getItem(arg2);
				mInterface.onReadBook(story);

			}
		});
		mListBook.setAdapter(new ArrayAdapter<Story>(mContext,
				android.R.layout.simple_list_item_1, stories) {
			@Override
			public View getView(final int position, View convertView,
					ViewGroup parent) {

				FoodViewItem fvi = (FoodViewItem) convertView;
				if (fvi == null)
					fvi = new FoodViewItem(getContext());
				final Story story = (Story) getItem(position);
				fvi.setData(story);
				fvi.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mInterface != null)
							mInterface.onReadBook(story);
					}
				});
				return fvi;

			}
		});
	}

	private DVInterface mInterface;

	public void setInterface(DVInterface _interface) {
		mInterface = _interface;
	}

	public static interface DVInterface {
		public void onReadBook(Story story);
	}
}
