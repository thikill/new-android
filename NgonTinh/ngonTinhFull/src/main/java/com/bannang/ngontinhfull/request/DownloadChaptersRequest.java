package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;

public class DownloadChaptersRequest extends BaseRequest {
	@XmlTag("ids")
	public String[] ids;
	public DownloadChaptersRequest() {
		super(getUrl("chapter/gets"));
	}

}
