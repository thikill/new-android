package com.bannang.ngontinhfull;

public interface ICallback {
	public Exception mEx = null;
	public boolean run(Object... obj);
}
