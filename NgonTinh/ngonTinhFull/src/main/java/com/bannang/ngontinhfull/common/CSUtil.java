package com.bannang.ngontinhfull.common;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class CSUtil {

	private static final int CREDIT_CART_LIMIT_TERM = 12;
	private static String pref_password = "";
	private static final SimpleDateFormat mSdf = new SimpleDateFormat(
			"yyyy/MM/dd hh:mm:ss");
	public static final SimpleDateFormat mSdfShort = new SimpleDateFormat(
			"MM/dd hh:mm");

	public static String getDeviceId(Context ctx) {
		return Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID);
	}

	public static void showKeyboard(View view, boolean bShow) {
		InputMethodManager imm = (InputMethodManager) view.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm == null)
			return;

		if (bShow)
			imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
		else {
			imm.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.RESULT_HIDDEN);
		}
	}

	/**
	 * On Android 2.2+
	 * 
	 * @param email
	 *            string for email address
	 * @return
	 */
	public static boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static String toString(String obj) {
		if (obj == null)
			return "";
		else
			return obj.trim();
	}

	public static boolean isTrueStringValue(String str) {
		if (str == null || str.length() <= 0)
			return false;
		if ("true".equalsIgnoreCase(str))
			return true;
		return false;
	}

	public static boolean isNullString(String str) {
		return str == null || str.length() <= 0;
	}

	public static String normalizeIntStr(String str) {
		return str.replaceAll("[^\\d.]", "");
	}

	public static Integer parseInt(String str) {
		if (str == null || str.length() <= 0)
			return 0;
		try {
			return Integer.parseInt(normalizeIntStr(str));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static Long parseLong(String str) {
		if (str == null || str.length() <= 0)
			return 0L;
		try {
			return Long.parseLong(normalizeIntStr(str));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0L;
	}

	public static double parseDouble(String str) {
		if (str == null || str.length() <= 0)
			return 0;
		try {
			str = str.replaceAll("[^\\d.]", "");
			return Double.parseDouble(str);

		} catch (NumberFormatException e) {

		}
		return 0;
	}

	public static String reformatPrice(String str) {
		if (str == null || str.length() <= 0)
			return "";
		try {
			str = str.replaceAll("[^\\d.]", "");
			double val = Double.parseDouble(str);
			DecimalFormat formatter = new DecimalFormat("###,###,###");
			return "??�" + formatter.format(val);

		} catch (NumberFormatException e) {

		}

		return "";
	}

	public static String readInputStreamAsString(InputStream in)
			throws IOException {
		BufferedInputStream bis = new BufferedInputStream(in);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result = bis.read();
		while (result != -1) {
			byte b = (byte) result;
			buf.write(b);
			result = bis.read();
		}
		return buf.toString();
	}

	public static InputStream string2Stream(String str) {
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
				str.getBytes());
		return byteArrayInputStream;
	}

	public static Object clone2(Object org) {
		if (org == null || org instanceof String || org instanceof Integer
				|| org instanceof Boolean || org instanceof Byte
				|| org instanceof Double)
			return org;
		if (org.getClass().isPrimitive())
			return org;
		Class<?> clazz = org.getClass();

		try {
			if (clazz.isArray()) {
				int length = Array.getLength(org);
				Object retArray = Array.newInstance(clazz.getComponentType(),
						length);
				for (int i = 0; i < length; i++) {
					Object crOrg = Array.get(org, i);
					Array.set(retArray, i, clone2(crOrg));
				}
				return retArray;
			} else {
				Object ret;
				ret = clazz.newInstance();
				for (Field field : clazz.getFields()) {
					int modifiers = field.getModifiers();
					if (Modifier.isFinal(modifiers))
						continue;
					field.set(ret, clone2(field.get(org)));
				}
				return ret;
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object xmlORM(InputStream inputStream, Class<?> clazz)
			throws Exception {

		InputSource source = new InputSource(inputStream);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document document = factory.newDocumentBuilder().parse(source);
		Element rootElement = document.getDocumentElement();
		if (rootElement == null || rootElement.getChildNodes() == null)
			return null;
		return mappingNode2Field(rootElement, clazz);

	}

	private static Field getFieldByTagName(Class<?> clazz, String tagName)
			throws Exception {

		Field[] declareFields = clazz.getDeclaredFields();
		for (Field field : declareFields) {
			XmlTag fTag = field.getAnnotation(XmlTag.class);
			if (fTag != null && tagName.equals(fTag.value())) {
				return field;
			}
		}
		Field[] fields = clazz.getFields();
		for (Field field : fields) {
			XmlTag fTag = field.getAnnotation(XmlTag.class);
			if (fTag != null && tagName.equals(fTag.value())) {
				return field;
			}
		}
		return null;
	}

	private static boolean invalidArrayNode(NodeList childNodeList) {
		int length = childNodeList.getLength();
		if (length <= 1)
			return false;
		for (int k = 0; k < length - 1; k++) {
			String kNodeName = childNodeList.item(k).getNodeName();
			if (kNodeName == null) {
				return true;
			}
			for (int h = k; h < length; h++) {
				if (kNodeName.equals(childNodeList.item(h).getNodeName())) {
					return true;
				}
			}
		}
		return false;

	}

	public static Object mappingNode2Field(Node node, Class<?> filedClass)
			throws Exception {
		// Leaf node
		if (String.class.equals(filedClass) || Boolean.class.equals(filedClass)
				|| Integer.class.equals(filedClass) || filedClass.isPrimitive()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element) node;
				return el.getTextContent();
			} else
				return null;
		}

		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node nodeChild = nodeList.item(i);
			if(nodeChild.getNodeType() == 3 && nodeChild.getNodeValue().trim().length()==0){
				node.removeChild(nodeChild);
			}
		}
		nodeList = node.getChildNodes();
		Object fieldValue = filedClass.newInstance();

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node nodeChild = nodeList.item(i);
			
			String tagName = nodeChild.getNodeName();
			Field fieldChild = getFieldByTagName(filedClass, tagName);
			if (fieldChild == null) {
				Node firstChild = nodeChild.getFirstChild();
				while (fieldChild == null && firstChild != null
						&& nodeChild.getChildNodes().getLength() > 0) {
					String nodeChildOfChildTagName = firstChild.getNodeName();
					fieldChild = getFieldByTagName(filedClass,
							nodeChildOfChildTagName);
					if (nodeChild.getChildNodes().getLength() == 1) {
						NodeList childNodeList = firstChild.getChildNodes();
						if (childNodeList != null) {
							if (!invalidArrayNode(childNodeList)) {
								nodeChild = firstChild;
							}
						}
					}
					firstChild = firstChild.getFirstChild();
				}
				if (fieldChild == null)
					continue;

				if (fieldChild.getType().isArray()) {
					Object valueOfField = mappingNode2Field(
							nodeChild.getChildNodes(), fieldChild.getType());
					fieldChild.set(fieldValue, valueOfField);
				} else {
					Object valueOfField = mappingNode2Field(firstChild,
							fieldChild.getType());
					fieldChild.set(fieldValue, valueOfField);
				}
			} else {
				if (fieldChild.getType().isArray()) {
					Object valueOfField = mappingNode2Field(nodeList,
							fieldChild.getType());
					fieldChild.set(fieldValue, valueOfField);
					return fieldValue;
				} else {
					Object valueOfField = mappingNode2Field(nodeChild,
							fieldChild.getType());
					fieldChild.set(fieldValue, valueOfField);
				}
			}
		}
		return fieldValue;

	}

	private static Object mappingNode2Field(NodeList nodes, Class<?> filedClass)
			throws Exception {

		if (filedClass.isArray()) {
			int nodeLength = nodes.getLength();
			Object fieldArrayValue = Array.newInstance(
					filedClass.getComponentType(), nodeLength);
			for (int i = 0; i < nodeLength; i++) {
				Object arrayItemValue = mappingNode2Field(nodes.item(i),
						filedClass.getComponentType());
				Array.set(fieldArrayValue, i, arrayItemValue);
			}
			return fieldArrayValue;
		}
		return null;

	}

	public static String Object2JSonString(Object obj, String rootName)
			throws Exception {
		if (rootName != null) {
			JSONObject json = new JSONObject();
			json.put(rootName, JSonORM(obj, rootName));
			return json.toString();
		} else {
			return JSonORM(obj, rootName).toString();
		}
	}

	public static Object JSonORM(Object obj, String tag) throws Exception {
		JSONObject json = new JSONObject();

		if (obj == null)
			return null;

		if (obj instanceof String || obj instanceof Integer
				|| obj instanceof Double || obj instanceof Boolean
				|| obj instanceof Long) {
			return obj;
		}
		Class<?> clazz = obj.getClass();
		if (clazz.isArray()) {
			int length = Array.getLength(obj);
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < length; i++) {
				jsonArray.put(JSonORM(Array.get(obj, i), ""));
			}
			return jsonArray;
		} else {

			Field[] fields = clazz.getFields();
			for (Field field : fields) {
				XmlTag fTag = field.getAnnotation(XmlTag.class);
				if (fTag == null)
					continue;
				json.put(fTag.value(), JSonORM(field.get(obj), fTag.value()));

			}
		}

		return json;
	}

	// Json String to Object
	public static Object JSonORM(String jsonStr) throws Exception {
		JSONObject json = new JSONObject(jsonStr);
		Iterator<String> keys = (Iterator<String>) json.keys();

		if (keys != null && keys.hasNext()) {

			while (keys.hasNext()) {
				String key = keys.next();
				Class<?> clazz = Class.forName(key);
				if (clazz == null)
					continue;
				return JSonORM(json.get(key), clazz);
			}

		}
		return null;
	}

	public static Object JSonORM(String jsonStr, Class<?> clazz)
			throws Exception {
		JSONObject json = new JSONObject(jsonStr);
		return JSonORM(json, clazz);
	}

	@SuppressWarnings("unchecked")
	public static Object JSonORM(Object json, Class<?> clazz) throws Exception {
		if (json == JSONObject.NULL)
			return null;
		// Kiem tra nut la
		JSONObject jsonObject = null;
		if (json instanceof JSONObject) {
			jsonObject = (JSONObject) json;
		}
		if (clazz.isPrimitive() || String.class.equals(clazz)
				|| Boolean.class.equals(clazz) || Integer.class.equals(clazz)
				|| Double.class.equals(clazz) || Long.class.equals(clazz)) {

			if (jsonObject != null) {
				Iterator<String> keys = jsonObject.keys();
				String key = null;
				if (keys != null && (key = keys.next()) != null) {
					if (String.class.equals(clazz)) {
						return jsonObject.getString(key);
					} else if (Integer.class.equals(clazz)) {
						return jsonObject.getInt(key);
					} else if (Boolean.class.equals(clazz)) {
						return jsonObject.getBoolean(key);
					} else if (Double.class.equals(clazz)) {
						return jsonObject.getDouble(key);
					} else if (Long.class.equals(clazz)) {
						return jsonObject.getLong(key);
					}
				}
			} else {
				return json;
			}
		} else {
			if (jsonObject == null) {

			}
		}
		if (jsonObject == null) {
			if (json instanceof JSONArray) {
				JSONArray jsonArray = (JSONArray) json;
				if (clazz.isArray()) {

					int length = jsonArray.length();
					Object retObject = Array.newInstance(
							clazz.getComponentType(), length);
					for (int i = 0; i < length; i++) {
						Array.set(
								retObject,
								i,
								JSonORM(jsonArray.get(i),
										clazz.getComponentType()));
					}
					return retObject;
				} else {
					Field[] fields = clazz.getFields();
					if (fields != null && fields.length > 0) {
						Class<?> arrayClass = fields[0].getType();
						int length = jsonArray.length();
						if (length == 0)
							return null;
						Object arrayObject = Array.newInstance(
								arrayClass.getComponentType(), length);
						for (int i = 0; i < length; i++) {
							Array.set(
									arrayObject,
									i,
									JSonORM(jsonArray.get(i),
											arrayClass.getComponentType()));
						}

						return arrayObject;
					} else {
						return json;
					}
				}
			} else {
				return json;
			}
		} else if (clazz.getFields() == null || clazz.getFields().length == 0) {
			return json;
		}
		// Duyet json
		Iterator<String> keys = (Iterator<String>) jsonObject.keys();

		Object retObject = clazz.newInstance();
		String key = keys.hasNext() ? keys.next() : null;

		while (key != null) {
			XmlTag classTag = clazz.getAnnotation(XmlTag.class);
			if (classTag != null && key.equals(classTag.value())) {
				return JSonORM(jsonObject.get(key), clazz);
			}
			// Tim field co tag tuong ung voi json
			Field field = getFieldByTagName(clazz, key);
			// Bo qua nhung field khong duoc khai bao
			if (field == null) {
				if (!keys.hasNext())
					return retObject;
				key = keys.next();
				continue;
			}
			field.setAccessible(true);
			// Neu field la 1 array
			if (field.getType().isArray()) {
				// Lay array cua json hien tai

				JSONArray jsonArray = jsonObject.getJSONArray(key);
				if (jsonArray == null) { // Neu khong phai la 1 array
					field.set(retObject, null);
				} else { // Xay dung toan bo array
					int length = jsonArray.length();
					Object fieldValue = Array.newInstance(field.getType()
							.getComponentType(), length);
					// Lay value cua toan bo json
					for (int i = 0; i < length; i++) {
						if (fieldValue != null)
							Array.set(
									fieldValue,
									i,
									JSonORM(jsonArray.get(i), field.getType()
											.getComponentType()));
					}
					if (fieldValue != null)
						field.set(retObject, fieldValue);
				}
			} else {
				// Truong hop la 1 object

				Object fieldValue = JSonORM(jsonObject.get(key),
						field.getType());
				if (fieldValue != null)
					field.set(retObject, fieldValue);

			}
			if (!keys.hasNext())
				return retObject;
			key = keys.next();
		}
		return retObject;
	}

	

	public static void CopyStream(InputStream is, OutputStream os)
	{
	    final int buffer_size=1024;
	    try
	    {
	        byte[] bytes=new byte[buffer_size];
	        for(;;)
	        {
	          int count=is.read(bytes, 0, buffer_size);
	          if(count==-1)
	              break;
	          os.write(bytes, 0, count);
	        }
	    }
	    catch(Exception ex){}
	}


	public static double getDistance(double fromLat, double fromLng, double toLat, double toLng) {
		double dy = Math.abs(fromLat-toLat) * 231 * 40 * 12;
		double dx = Math.abs(fromLng-toLng) * 8 * 279 * 40;
		return Math.sqrt(dx*dx+dy*dy);
	}

	public static String getPrefPassword(Context ctx) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(ctx);

		String pd1 = prefs.getString(Constants.PREFS_PASSWORD, pref_password);
		String pd2 = prefs.getString(Constants.PREFS_PASSWORDVD, pref_password);
		if (pd1.equals(pd2)) {
			pref_password = pd1;
		} else {
			pref_password = "";
		}		
		return pref_password;
	}	
	
	
	
	public static int getBottomMargin( Context ctx ) {
		WindowManager wm = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE));
		Display mDisplay = wm.getDefaultDisplay();
		Configuration mConfig = ctx.getResources().getConfiguration();
        
        // Screen Size classification
		int mSizeClass = mConfig.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;	
		
		switch ( mSizeClass ) {
		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			return 50;
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			return 60;
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			return 100;
		case Configuration.SCREENLAYOUT_SIZE_XLARGE:
			return 100;
		case Configuration.SCREENLAYOUT_SIZE_UNDEFINED:
			return 50;
		}
		return 70;
				
	}
}