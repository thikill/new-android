package com.bannang.ngontinhfull.views.dialog;

import android.app.Dialog;
import android.content.Context;

import com.bannang.ngontinhfull.R;

public class ChooseOption extends Dialog {

	public ChooseOption(Context context) {
		super(context);
		setContentView(R.layout.choose_font_size_dlg);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
	}

}
