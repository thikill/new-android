package com.bannang.ngontinhfull.request;

import com.bannang.ngontinhfull.common.XmlTag;
import com.bannang.ngontinhfull.models.Chapter;

public class DownloadChapterRequest extends BaseRequest {
	@XmlTag("chapter")
	public Chapter chapter; 
	public DownloadChapterRequest() {
		super(getUrl("chapter/get"));
	}

}
