package com.bannang.ngontinhfull.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.Layout;
import android.util.TypedValue;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.ICallback;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.CSUtil;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.Logger;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Chapter;
import com.bannang.ngontinhfull.models.Page;
import com.bannang.ngontinhfull.views.pagecurl.CurlView;
import com.bannang.ngontinhfull.views.pagecurl.CurlView.PageViewInterface;

public class ReaderContentView extends BaseLinearLayout implements
		PageViewInterface, OnGlobalLayoutListener {
	private Chapter mChap;
	private TextView mTvContent;
	private CurlView mCurlView;
	private int mDisplayHeight = 800;
	private int MARGIN = 100;
	private int mCatID;	
	public ReaderContentView(Context context) {
		super(context, R.layout.reader_content);
		MARGIN = CSUtil.getBottomMargin(context);
		mTvContent = (TextView) findViewById(R.id.tvContent);
		mTvContent.setPadding(40, 20, 20, 20);
		getViewTreeObserver().addOnGlobalLayoutListener(this);
		mCurlView = (CurlView) findViewById(R.id.curl);
	}

	public void setData(Chapter chap, int catID) {
		mChap = chap;
		mCatID = catID;
		if (mChap.content != null)
			loadContent();
		else {
			// readFormServer(mChap);
			readFromDropBox(mChap);
		}
	}

	private void readFromDropBox(final Chapter chap) {
		Static.cancellAllTask();
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			private Exception mEx = null;
			private ProgressDialog mProgress;

			@Override
			protected void onPreExecute() {
				if (((Activity) mContext).isFinishing() == false) {
					mProgress = new ProgressDialog(mContext);
					mProgress.setMessage(mContext
							.getString(R.string.loading_data));
					mProgress.show();
				}
			}

			@Override
			protected Void doInBackground(Void... params) {

				String filePath = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + File.separator+ Constants.DOWNLOAD_FOLDER+ File.separator;

				File f = new File(filePath);

				try {

					byte[] buffer = new byte[1024];
					int len = 0;
					ByteArrayBuffer bb = null;
					File existFile = new File(filePath + "c/" + chap.id);
					if (existFile.exists()) {
						FileInputStream fis = new FileInputStream(filePath
								+ "c/" + chap.id);
						bb = new ByteArrayBuffer((int)existFile.length());
						while ((len = fis.read(buffer)) > 0) {
							bb.append(buffer, 0, len);
						}
						fis.close();
					} else {
						URL url = new URL(
								"https://dl.dropboxusercontent.com/u/223793945/chapter/"+ mCatID+"/"
										+ chap.id);
						URLConnection conn = url.openConnection();
						InputStream is = conn.getInputStream();
						bb = new ByteArrayBuffer(conn.getContentLength());
						while ((len = is.read(buffer)) > 0) {
							bb.append(buffer, 0, len);
						}
						is.close();
						File directory = new File(filePath);
						if (directory.exists()) {
							filePath += "c/";
							directory = new File(filePath);
							if (!directory.exists())
								directory.mkdir();
							FileOutputStream fos = new FileOutputStream(
									filePath + chap.id);
							fos.write(bb.buffer());
							fos.close();
							chap.content = Static.decompress(bb.buffer());
						}
					}
					chap.content = Static.decompress(bb.buffer());

				} catch (Exception e) {
					try {
						f = new File(filePath + chap.id);
						if (f.exists())
							f.delete();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					mEx = e;
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				if (mProgress != null && mProgress.isShowing())
					mProgress.dismiss();
				if (mChap.content != null)
					loadContent();
				if (mEx != null) {
					if (mInterface != null)
						mInterface.onNetworkError(mEx);
				}
			}
		};
		Utils.executeAsyncTask(task);
	}

	private void loadContent() {
		if (mChap == null || mChap.content == null)
			return;
		getPages(new ICallback() {

			@Override
			public boolean run(Object... obj) {
				List<Page> pages = (List<Page>) obj[0];
				int iCurrentPage = (Integer) obj[1];
				mCurlView.setPageProvider(new PageProvider(mContext, pages));
				mCurlView.setCurrentIndex(iCurrentPage);
				mCurlView.setPageViewInterface(ReaderContentView.this);
				mCurlView.setSizeChangedObserver(new SizeChangedObserver());
				mCurlView.setBackgroundColor(0xFF202830);

				return true;
			}
		});
	}

	private List<Page> getPages(final ICallback callback) {
		final List<Page> fList = new ArrayList<Page>();

		mTvContent.setTextSize(TypedValue.COMPLEX_UNIT_DIP, Static.textSize);
		mTvContent.setText(Html.fromHtml(new String(mChap.content)));

		mTvContent.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					@SuppressLint("NewApi")
					@Override
					public void onGlobalLayout() {

						Layout layout = mTvContent.getLayout();
						if (layout == null)
							return;
						if (Build.VERSION.SDK_INT < 16) {

							mTvContent.getViewTreeObserver()
									.removeGlobalOnLayoutListener(this);
						} else {
							mTvContent.getViewTreeObserver()
									.removeOnGlobalLayoutListener(this);

						}
						int count = layout.getLineCount();
						int lineHeigh = mTvContent.getLineHeight();
						int margin = (int) TypedValue.applyDimension(
								TypedValue.COMPLEX_UNIT_DIP, MARGIN, mContext
										.getResources().getDisplayMetrics());
						int numLinePP = (mDisplayHeight - (margin + lineHeigh / 2))
								/ lineHeigh;
						Logger.e("LineHeigh = " + lineHeigh + "  Bound = "
								+ mDisplayHeight + " Num Line = " + numLinePP);

						int iLine = 0;
						int iCurrentPage = 0;
						// int iStartChar = 0;
						String content = mTvContent.getText().toString();
						while (iLine < count) {
							int iStart = layout.getLineStart(iLine);
							int iEnd = iStart;
							if (iLine + numLinePP >= count) {
								String page1Content = content.subSequence(
										iStart, content.length()).toString();
								iEnd = content.length();
								fList.add(new Page(page1Content, iStart,
										content.length()));
								break;
							} else {
								iEnd = layout.getLineEnd(iLine + numLinePP);
								// iEnd = content.indexOf(".", iEnd);
								String page1Content = content.substring(iStart,
										iEnd).toString();
								fList.add(new Page(page1Content, iStart, iEnd));
								// iStartChar = iEnd+1;
								iLine += numLinePP + 1;
							}
							if (Static.currentStartPage != null) {
								if (iStart <= Static.currentStartPage.iStart
										&& Static.currentStartPage.iStart < iEnd) {
									iCurrentPage = fList.size() - 1;
									// Logger.e("iStart = "+iStart+"<= currentStartPage.iStart = "
									// + Static.currentStartPage.iStart
									// +" < iEnd = "+iEnd);
									// Logger.e("Page iStart = "+fList.get(fList.size()-1).iStart+" < Page iEnd = "+fList.get(fList.size()-1).iEnd);
									// Static.currentStartPage =
									// fList.get(fList.size()-1);
									// int firstSpaceIndex =
									// content.indexOf(" ",Static.currentStartPage.iStart);
									// Static.currentStartPage.startWord =
									// content.substring(Static.currentStartPage.iStart,
									// firstSpaceIndex);
									// Static.currentStartPage.content.replace(Static.currentStartPage.startWord,
									// "%%%%%");
								}
							}
						}
						callback.run(fList, iCurrentPage);
					}
				});
		return fList;
	}

	private class SizeChangedObserver implements CurlView.SizeChangedObserver {
		@Override
		public void onSizeChanged(int w, int h) {
			if (w > h) {
				mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
				mCurlView.setMargins(.1f, .05f, .1f, .05f);
			} else {
				mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
				mCurlView.setMargins(.05f, .01f, .01f, .01f);
			}
		}
	}

	@Override
	public void updateLast() {
		if (mInterface != null)
			mInterface.readLast();
	}

	private ReaderInterface mInterface;

	public void setReaderInterface(ReaderInterface _interface) {
		mInterface = _interface;
	}

	public static interface ReaderInterface {
		public void readLast();

		public void onNetworkError(Exception e);

	}

	@SuppressLint("NewApi")
	@Override
	public void onGlobalLayout() {

		if (getMeasuredHeight() <= 0)
			return;
		if (Build.VERSION.SDK_INT < 16) {
			mTvContent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
		} else {
			mTvContent.getViewTreeObserver().removeOnGlobalLayoutListener(this);

		}
		mDisplayHeight = getMeasuredHeight();
	}

}
