package com.bannang.ngontinhfull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;

import com.actionbarsherlock.view.Menu;
import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.MainActivity;
import com.bannang.ngontinhfull.MainContentFrag;
import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.MainMenuFrag.OnCategorySelectedListener;
import com.bannang.ngontinhfull.common.CSUtil;
import com.bannang.ngontinhfull.common.ConfigUtils;
import com.bannang.ngontinhfull.common.Constants;
import com.bannang.ngontinhfull.common.Utils;
import com.bannang.ngontinhfull.models.Category;
import com.bannang.ngontinhfull.models.PartnerConfig;
import com.bannang.ngontinhfull.views.dialog.LockAppDialog;
import com.bannang.ngontinhfull.views.dialog.LoginDialog;
import com.bannang.ngontinhfull.views.dialog.LockAppDialog.LockAppInterface;
import com.bannang.ngontinhfull.views.dialog.LoginDialog.LoginInterface;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;

public class MainActivity extends SlidingFragmentActivity implements
		OnCategorySelectedListener, LoginInterface, LockAppInterface {
	protected MainMenuFrag mMenu;
	private Bundle mSavedInstanceSate = null;
	private boolean isLogged = false;
	public Menu mOptionMenu = null;
	public int menuType = 0;
	static AsyncTask<Void, Void, Void> downloadTask;
	public static boolean isDownloading = true;
	final Context context = this;
	public static MainActivity INSTANCE;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Utils.mkAppFoler();
		Static.getDB(context);

		Static.sGaInstance = GoogleAnalytics.getInstance(this);

		// Use the GoogleAnalytics singleton to get a Tracker.
		Static.TRACKER = Static.sGaInstance.getTracker(Static.GA_ID);

		Static.TRACKER.sendView("MainActivity");
		mSavedInstanceSate = savedInstanceState;
		isLogged = false;
		setBehindContentView(R.layout.menu_bar);

		StartAppAd.init(MainActivity.this, "110314636", "212632616");
		StartAppSearch.init(MainActivity.this, "110314636", "212632616");
		setContentView(R.layout.content_bar);
		if (Static.getInfo().getStartAppEnable1()) {
			StartAppSearch.showSearchBox(MainActivity.this);
		}
		menuType = 0;
		// Contexts.instance().initActivity(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSlidingActionBarEnabled(true);
		if ("".equals(CSUtil.getPrefPassword(this))) {
			initLayout();
			return;
		}

		LoginDialog loginDlg = new LoginDialog(this);
		loginDlg.setmLoginInterface(this);
		loginDlg.show();		
		
	}

	@Override
	public void onCategorySelected(Category cat) {
		if (Static.TRACKER != null)
			Static.TRACKER.sendEvent("MainMenu", "Select Chapter", cat.name,
					null);
		MainContentFrag contentFrag = (MainContentFrag) getSupportFragmentManager()
				.findFragmentById(R.id.content_bar);
		if (contentFrag == null)
			return;
		contentFrag.setData(cat);
		getSlidingMenu().showContent(true);
		getSupportActionBar().setTitle(cat.name);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		switch (menuType) {
		case 0:
			getSupportMenuInflater().inflate(R.menu.main, menu);
			break;
		case 1:
			getSupportMenuInflater().inflate(R.menu.story, menu);
			break;
		case 2:
			getSupportMenuInflater().inflate(R.menu.history, menu);
			break;
		case 3:
			getSupportMenuInflater().inflate(R.menu.download, menu);
			break;
		default:
			break;
		}

		mOptionMenu = menu;
		return true;
	}

	@Override
	public void onPostCreate(Bundle savedInstanceState) {
		if (savedInstanceState == null && isLogged) {
			savedInstanceState = new Bundle();
			savedInstanceState.putBoolean("SlidingActivityHelper.open", true);
		}
		super.onPostCreate(savedInstanceState);
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {

		MainContentFrag contentFrag = (MainContentFrag) getSupportFragmentManager()
				.findFragmentById(R.id.content_bar);
		if (contentFrag == null)
			return true;
		switch (item.getItemId()) {
		case android.R.id.home:
			toggle();
			getSupportActionBar().setTitle(R.string.app_name);
			return true;

		case R.id.histoyBackHome:
			contentFrag.showDashBoard();
			menuType = 0;
			if (Build.VERSION.SDK_INT >= 11) {
				invalidateOptionsMenu();
			}
			getSupportActionBar().setTitle(R.string.app_name);
			return true;

		case R.id.downloadBackHome:
			contentFrag.showDashBoard();
			menuType = 0;
			if (Build.VERSION.SDK_INT >= 11) {
				invalidateOptionsMenu();
			}
			getSupportActionBar().setTitle(R.string.app_name);
			return true;
		case R.id.author:
			// isStoryView = true;
			MainContentFrag.INSTANCE.onAuthor();
			// toggle();
			menuType = 1;
			if (Build.VERSION.SDK_INT >= 11) {
				invalidateOptionsMenu();
			}
			return true;
		case R.id.action_settings:
			/*
			 * Intent intent = new Intent(this, PrefsActivity.class);
			 * this.startActivity(intent);
			 */
			LockAppDialog lockAppDlg = new LockAppDialog(MainActivity.this);
			lockAppDlg.setmLockAppInterface(MainActivity.this);
			lockAppDlg.show();
			return true;
		case R.id.action_dashboard:

			if (Static.TRACKER != null)
				Static.TRACKER.sendEvent("MainMenu", "Dash board", "", null);

			contentFrag.showDashBoard();
			menuType = 0;
			if (Build.VERSION.SDK_INT >= 11) {
				invalidateOptionsMenu();
			}
			getSupportActionBar().setTitle(R.string.app_name);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void cancel() {
		finish();
	}

	@Override
	public void success() {
		isLogged = true;
		initLayout();
		Handler hander = new Handler();
		hander.postDelayed(new Runnable() {

			@Override
			public void run() {
				getSlidingMenu().showMenu(true);
			}
		}, 1000);

	}

	private void initLayout() {
		if (mSavedInstanceSate == null) {
			FragmentTransaction t = this.getSupportFragmentManager()
					.beginTransaction();
			mMenu = new MainMenuFrag();
			t.replace(R.id.menu_bar, mMenu);
			if (isFinishing() == false) {
				t.commit();
			} else {
				t.commitAllowingStateLoss();
			}
		} else {
			mMenu = (MainMenuFrag) this.getSupportFragmentManager()
					.findFragmentById(R.id.menu_bar);
		}
		mMenu.setmOnCategorySelectedListener(this);
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

		if (isFinishing() == false) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_bar, new MainContentFrag()).commit();
		} else {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_bar, new MainContentFrag())
					.commitAllowingStateLoss();
		}

	}

	@Override
	public void cancelLockApp() {
		finish();
	}

	@Override
	public void okLockApp() {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// try {
		// Static.getDB(this).backupDatabase();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Static.release();
		super.finish();
	}

	@Override
	protected void onDestroy() {

		Static.release();
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// startAppAd.onBackPressed();
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

			new AlertDialog.Builder(this)
					.setIcon(R.drawable.ic_app)
					.setTitle(R.string.Application_Confirm_ExitTitle)
					.setMessage(R.string.Application_Confirm_ExitMessage)
					.setPositiveButton(R.string.Application_ConfirmExitOK,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									showAdd(Static.getInfo()
											.getStartAppEnable2());
									finish();
								}
							})
					.setNegativeButton(R.string.Application_ConfirmExitCancel,
							null).show();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		startAppAd.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (Static.getInfo().getStartAppEnable3()) {
			startAppAd.onPause();
		}
	}

	private Runnable mCloseAdd = new Runnable() {
		@Override
		public void run() {
			startAppAd.close();
		}
	};
	private StartAppAd startAppAd = new StartAppAd(this);
	private Handler mHandler;

	private void showAdd(boolean bShow) {

		if (!bShow)
			return;

		startAppAd.showAd();
		startAppAd.loadAd();

		if (mHandler == null) {
			mHandler = new Handler();
		}
		mHandler.postDelayed(mCloseAdd, 10000);

	}

}
