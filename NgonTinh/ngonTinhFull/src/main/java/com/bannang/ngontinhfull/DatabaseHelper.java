package com.bannang.ngontinhfull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.bannang.ngontinhfull.R;
import com.bannang.ngontinhfull.models.Category;
import com.bannang.ngontinhfull.models.Chapter;
import com.bannang.ngontinhfull.models.Download;
import com.bannang.ngontinhfull.models.Favorite;
import com.bannang.ngontinhfull.models.History;
import com.bannang.ngontinhfull.models.Story;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private String DB_PATH;
	private String mDbName;

	private Dao<Category, Integer> categoryDao = null;

	private Dao<Story, Integer> storyDao = null;
	private Dao<History, Integer> historyDao = null;
	private Dao<Download, Integer> downloadDao = null;
	private Dao<Favorite, Integer> favoriteDao = null;
	private Dao<Chapter, Integer> chapterDao = null;
	private Context mContext;

	public DatabaseHelper(Context context, String dbName) {
		super(context, dbName, null, 1);
		mContext = context;

		mDbName = dbName;
		DB_PATH = context.getDatabasePath(dbName).getAbsolutePath();
		Log.e("DB_PATH", DB_PATH);
	}

	public void createUserTable() throws SQLException {
		TableUtils.createTableIfNotExists(getConnectionSource(), History.class);
		TableUtils
				.createTableIfNotExists(getConnectionSource(), Favorite.class);
		TableUtils
		.createTableIfNotExists(getConnectionSource(), Download.class);
	}

	@Override
	public void close() {
		super.close();
	}

	public void backupDatabase() throws IOException {

		InputStream myInput = new FileInputStream(DB_PATH);
		String outFileName = Environment.getExternalStorageDirectory()
				+ "/test/";
		File file = new File(outFileName);
		if (!file.exists())
			file.mkdir();
		outFileName += mDbName;

		OutputStream myOutput = new FileOutputStream(outFileName);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void createTable() {
		createTable(Category.class);
		createTable(Chapter.class);
		createTable(Story.class);
	}

	public void initDb(boolean override) throws Exception {

		File fOut = new File(DB_PATH);
		if (fOut.exists() && !override)
			return;
		getReadableDatabase();

		InputStream f = mContext.getResources().openRawResource(R.raw.ngontinhfull);
		new ZipDecompress(f, DB_PATH).unzip(DB_PATH);
		f.close();
	}

	public Dao<History, Integer> getHistoryDao() throws SQLException {
		if (historyDao == null) {
			historyDao = getDao(History.class);
		}
		return historyDao;
	}

	public Dao<Story, Integer> getStoryDao() throws SQLException {
		if (storyDao == null) {
			storyDao = getDao(Story.class);
		}
		return storyDao;
	}

	public Dao<Category, Integer> getCategoryDao() throws SQLException {
		if (categoryDao == null) {
			categoryDao = getDao(Category.class);
		}
		return categoryDao;
	}
	public Dao<Download, Integer> getDownloadDao() throws SQLException {
		if (downloadDao == null) {
			downloadDao = getDao(Download.class);
		}
		return downloadDao;
	}
	
	
	public Dao<Favorite, Integer> getFavoriteDao() throws SQLException {
		if (favoriteDao == null) {
			favoriteDao = getDao(Favorite.class);
		}
		return favoriteDao;
	}
	
	public Dao<Chapter, Integer> getChapterDao() throws SQLException {
		if (chapterDao == null) {
			chapterDao = getDao(Chapter.class);
		}
		return chapterDao;
	}	
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource arg1) {

	}

	public void createTable(Class<?> clazz) {
		try {

			TableUtils.createTableIfNotExists(getConnectionSource(), clazz);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void clearTable(Class<?> clazz) {
		try {
			TableUtils.clearTable(connectionSource, clazz);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {

	}

}
