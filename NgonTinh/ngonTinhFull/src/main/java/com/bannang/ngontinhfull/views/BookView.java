package com.bannang.ngontinhfull.views;

import android.content.Context;
import android.widget.ListView;

import com.bannang.ngontinhfull.R;

public class BookView extends BaseLinearLayout {
	public ListView mListViewBooks;

	public BookView(Context context) {
		super(context, R.layout.book_view);
		mListViewBooks = (ListView) findViewById(R.id.listViewBooks);
	}

}
