package com.bannang.ngontinhfull.models;

import com.bannang.ngontinhfull.common.XmlTag;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class History {
	@XmlTag("id")
	@DatabaseField(generatedId=true)
	public Integer id;
	@XmlTag("bookId")
	@DatabaseField(columnName = "book_id")
	public Integer bookId;
	@DatabaseField
	public Integer readCount=1;
	@XmlTag("readTime")
	@DatabaseField(columnName = "read_time",dataType = DataType.DATE_LONG)
	public java.util.Date readTime;
	@XmlTag("curChapId")
	@DatabaseField(columnName = "cur_chap_id",defaultValue ="-1")
	public Integer curChapId;
	@XmlTag("curCharIndex")
	@DatabaseField(columnName = "cur_char_index",defaultValue="0")
	public Integer curCharIndex;
}
