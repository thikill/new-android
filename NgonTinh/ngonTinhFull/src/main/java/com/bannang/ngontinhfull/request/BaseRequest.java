package com.bannang.ngontinhfull.request;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.bannang.ngontinhfull.Static;
import com.bannang.ngontinhfull.common.CSUtil;
import com.bannang.ngontinhfull.response.BaseResponse;

public class BaseRequest {
	private static final String SERVER_URL = "http://choi24.com/storyapp/";
	private String _url;
	public Object saveFilePath = null;

	public BaseRequest(String url) {
		_url = url;
	}

	public Object postRequest(Class<?> clazz) throws Exception {

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(_url);
//		 Log.e("Chinh","URL = "+_url);
		HttpParams httpParamsObj = httpclient.getParams();

		HttpConnectionParams.setConnectionTimeout(httpParamsObj,
				Static.sConnectTimeout);
		HttpConnectionParams.setSoTimeout(httpParamsObj,
				Static.sSokectTimeout);

		List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(1);
		String jsonRequestStr = CSUtil.Object2JSonString(this, null);
		 Log.e("Chinh","Post = "+jsonRequestStr);
		nameValuePair.add(new BasicNameValuePair("JSON_DATA", jsonRequestStr));

		httppost.setEntity(new UrlEncodedFormEntity(nameValuePair, "utf-8"));
		HttpResponse response = httpclient.execute(httppost);

		BaseResponse resp = (clazz != null) ? (BaseResponse) clazz
				.newInstance() : new BaseResponse();
		resp.headers = response.getAllHeaders();
		int responseCode = response.getStatusLine().getStatusCode();
		if (responseCode == HttpStatus.SC_OK) {
			InputStream in = response.getEntity().getContent();
			if (in == null) {
				return resp;
			}
			if (clazz != null) {
				// convert to object
				StringBuilder totalConent = new StringBuilder();
				BufferedReader rd = new BufferedReader(new InputStreamReader(
						in, "utf-8"));
				String line = null;
				while ((line = rd.readLine()) != null) {
					totalConent.append(line);
				}

				resp = (BaseResponse) CSUtil.JSonORM(
						totalConent.toString(), clazz);
				in.close();
			} else {
				resp.rawData = in;
			}
		}

		return resp;

	}

	public static String getUrl(String url) {
		return SERVER_URL + url;
	}

}
